﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security;
using System.Security.Cryptography;

namespace BOSGATE
{
    public class SecurityController
    {
        public string Password_Encode(string pass)
        {
            string mesh ="SmartlifeSmileGuys";

            var hash = SHA1.Create();
            var encoder = new ASCIIEncoding();
            var combined = encoder.GetBytes(pass ?? "");
            string fin = BitConverter.ToString(hash.ComputeHash(combined)).ToLower().Replace("-", "");
            return fin;
        }
    }
}
