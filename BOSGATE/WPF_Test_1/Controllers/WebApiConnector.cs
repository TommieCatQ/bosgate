﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;


namespace BOSGATE
{
    class WebAPIResponse
    {
        public bool error { get; set; }
        public string message { get; set; }
    }

    public class WebApiConnector
    {
        private string InitialConnectionString = "http://192.168.1.133:80/bosgate_site/v1";

        private T General_GET<T>(string URI)
        {
            var client = new RestClient(InitialConnectionString + URI);
            var request = new RestRequest(Method.GET);
            string rawData = "";
            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            IRestResponse response = client.Execute(request);
            rawData = response.Content;
            T thisRoot = SimpleJson.DeserializeObject<T>(rawData);
            return thisRoot;
        }

        #region FakeMethod
        public List<User> getUserTable_fake()
        {
            List<User> usertable = new List<User>();
            //usertable.Add(new User(1, "Long", "longnguyen@gmail.com", "01Jan1988", 10011, 0, "18Feb2017"));
            //usertable.Add(new User(2, "Tâm", "tamtran@gmail.com", "02Jan1988", 10012, 1, "18Feb2017"));
            //usertable.Add(new User(3, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 1, "18Feb2017"));
            //usertable.Add(new User(4, "Minh", "minhvo@gmail.com", "14Jul1990", 100134, 2, "18Feb2017"));
            //usertable.Add(new User(5, "Huan", "huanhuan@gmail.com", "20Oct1988", 10015, 2, "18Feb2017"));
            //usertable.Add(new User(6, "Tai", "taivo@gmail.com", "21Dec1990", 10016, 2, "18Feb2017"));
            //usertable.Add(new User(7, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 2, "18Feb2017"));
            //usertable.Add(new User(8, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 2, "18Feb2017"));
            //usertable.Add(new User(9, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 2, "18Feb2017"));
            //usertable.Add(new User(10, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 2, "18Feb2017"));
            //usertable.Add(new User(11, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 2, "18Feb2017"));
            //usertable.Add(new User(12, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 2, "18Feb2017"));
            //usertable.Add(new User(13, "Quân", "maivietquan@gmail.com", "19Aug1992", 10013, 2, "18Feb2017"));

            return usertable;
        }

        public User getChoosenUser_fake(int id)
        {
            User choosen = new User();

            // get all user
            List<User> users = getUserTable_fake();

            // get the user with specific id in database
            choosen = users.Find(x => x.Id == id);

            return choosen;
        }
        #endregion

        #region USERS methods

        //User register
        public string User_Register(string name, string email, string password)
        {
            var client = new RestClient(InitialConnectionString + "/login");
            var request = new RestRequest(Method.POST);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("name", name, ParameterType.UrlSegment);
            request.AddParameter("email", email, ParameterType.UrlSegment);
            request.AddParameter("password", password, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            WebAPIResponse webApiResp = (WebAPIResponse)SimpleJson.DeserializeObject<WebAPIResponse>(response.Content);

            return webApiResp.message;
        }


        //User login
        public void User_Login(string email, string password)
        {
            var client = new RestClient(InitialConnectionString + "/login");
            var request = new RestRequest(Method.POST);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("email", email, ParameterType.UrlSegment);
            request.AddParameter("password", password, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
        }

        // Get all users data from Web API 
        public Root_User Users_GET()
        {
            Root_User UserList = General_GET<Root_User>("/email");
            return UserList;
        }

        // put this user to database with Wep API
        public void User_PUT(User user)
        {
            var client = new RestClient(InitialConnectionString + "/email");
            var request = new RestRequest(Method.PUT);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("name", user.Username, ParameterType.UrlSegment);
            request.AddParameter("birthday", user.Birthday, ParameterType.UrlSegment);
            request.AddParameter("empid", user.EmpID, ParameterType.UrlSegment);
            request.AddParameter("directmanager", user.DirectManager, ParameterType.UrlSegment);
            request.AddParameter("status", user.Status, ParameterType.UrlSegment);
            request.AddParameter("created_at", user.Created_at, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
        }

        #endregion

        #region Standard methods
        // POST new standard
        public void Standard_POST(Standard standard)
        {
            var client = new RestClient(InitialConnectionString + "/standards");
            var request = new RestRequest(Method.POST);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("day_old", standard.day_old, ParameterType.UrlSegment);
            request.AddParameter("wgain", standard.weight_gain, ParameterType.UrlSegment);
            request.AddParameter("wstd", standard.weight_std, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
        }

        // put a standard with Wep API
        public void Standard_PUT(Standard standard)
        {
            var client = new RestClient(InitialConnectionString + "/standards/" + standard.id.ToString());
            var request = new RestRequest(Method.PUT);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("day_old", standard.day_old, ParameterType.UrlSegment);
            request.AddParameter("wgain", standard.weight_gain, ParameterType.UrlSegment);
            request.AddParameter("wstd", standard.weight_std, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
        }

        #endregion

        #region Customers methods
        // Customer Register
        public string Customer_REGISTER(Customer customer)
        {
            var client = new RestClient(InitialConnectionString + "/add_Customer");
            var request = new RestRequest(Method.POST);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("customerid", customer.CustomerID, ParameterType.UrlSegment);
            request.AddParameter("name", customer.Name, ParameterType.UrlSegment);
            request.AddParameter("phone", customer.Phone, ParameterType.UrlSegment);
            request.AddParameter("email", customer.Email, ParameterType.UrlSegment);
            request.AddParameter("password", customer.Password_hash, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            WebAPIResponse webApiResp = (WebAPIResponse)SimpleJson.DeserializeObject<WebAPIResponse>(response.Content);

            return webApiResp.message;
        }

        // Customer Login
        public Customer Customer_LOGIN(string CustID, string Password)
        {
            var client = new RestClient(InitialConnectionString + "/CustomerLogin");
            var request = new RestRequest(Method.POST);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("customerid", CustID, ParameterType.UrlSegment);
            request.AddParameter("password", Password, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
            Customer customer = (Customer)SimpleJson.DeserializeObject<Customer>(response.Content);
            return customer;
        }


        // Get all Standard data from Web API 
        public Root_Customer Customers_GET()
        {
            Root_Customer CustomerList = General_GET<Root_Customer>("/Customers");
            return CustomerList;
        }

        // change Password of Customer
        public void Customer_change_PASSWORD(int CustID, string curr_pass, string new_pass)
        {
            var client = new RestClient(InitialConnectionString + "/CustomerPassword/" + CustID.ToString());
            var request = new RestRequest(Method.PUT);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("curr_password", curr_pass, ParameterType.UrlSegment);
            request.AddParameter("new_password", new_pass, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
        }

        // edit customer info
        public void Customer_Edit(Customer cust)
        {
            var client = new RestClient(InitialConnectionString + "/updateCustomer/" + cust.CustomerID.ToString());
            var request = new RestRequest(Method.PUT);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("name", cust.Name, ParameterType.UrlSegment);
            request.AddParameter("phone", cust.Phone, ParameterType.UrlSegment);
            request.AddParameter("email", cust.Email, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);

        }

        #endregion

        #region Devices method
        // POST new standard
        public void Device_POST(Device device)
        {
            var client = new RestClient(InitialConnectionString + "/add_device");
            var request = new RestRequest(Method.POST);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("DeviceID", device.DeviceID, ParameterType.UrlSegment);
            request.AddParameter("CustomerID", device.FarmID, ParameterType.UrlSegment);
            request.AddParameter("HW_Ver", device.HWVer, ParameterType.UrlSegment);
            request.AddParameter("SW_Ver", device.SWVer, ParameterType.UrlSegment);
            request.AddParameter("Address", device.Address, ParameterType.UrlSegment);
            request.AddParameter("Device_Name", device.DeviceName, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
        }

        // Get all Standard data from Web API 
        public Root_Device Device_GET()
        {
            Root_Device DeviceList = General_GET<Root_Device>("/devices");
            return DeviceList;
        }

        // put a standard with Wep API
        public void Device_PUT(Device device)
        {
            var client = new RestClient(InitialConnectionString + "/updateDevice/" + device.DeviceID.ToString());
            var request = new RestRequest(Method.PUT);

            request.AddHeader("authorization", "411142fd624ee4de219f80318e7b8dc9");
            request.AddHeader("SmartLife", DateTime.Now.ToString());
            request.AddParameter("DeviceID", device.DeviceID, ParameterType.UrlSegment);
            request.AddParameter("CustomerID", device.FarmID, ParameterType.UrlSegment);
            request.AddParameter("HW_Ver", device.HWVer, ParameterType.UrlSegment);
            request.AddParameter("SW_Ver", device.SWVer, ParameterType.UrlSegment);
            request.AddParameter("Address", device.Address, ParameterType.UrlSegment);
            request.AddParameter("Device_Name", device.DeviceName, ParameterType.UrlSegment);
            IRestResponse response = client.Execute(request);
        }
        #endregion

        #region Cows method
        #endregion
    }
}
