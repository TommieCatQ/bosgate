﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Threading;

namespace BOSGATE
{
    public class DatabaseController
    {
        #region PRIVATE METHODS
        private MySqlConnection connection;
        private string server;
        private string port;
        private string database;
        private string uid;
        private string password;
        private string charset;

        private string LocalConnection()
        {
            server = "localhost";
            port = "3307";
            database = "bosgate_db";
            uid = "root";
            password = "root";

            string cnString = "SERVER=" + server + ";" +
                                "PORT=" + port + ";" +
                                "DATABASE=" + database + ";" +
                                "UID=" + uid + ";" +
                                "PASSWORD=" + password + ";" +
                                "Convert Zero Datetime=True";
            return cnString;
        }

        private string ServerConnection()
        {
            server = "210.86.239.119";
            database = "konned";
            uid = "konned";
            password = "Konned@123";
            charset = "utf8";
            //server = "192.168.1.166";
            //database = "bosgate_db";
            //uid = "root";
            //password = "root";

            string cnString =   "SERVER=" + server + ";" +
                                "DATABASE=" + database + ";" + 
                                "UID=" + uid + ";" +
                                "charset=" + charset + ";" +
                                "PASSWORD=" + password + ";" + 
                                "Convert Zero Datetime=True";
            return cnString;
        }

        public DatabaseController(int connectionType)
        {
            string connectionString = "";
            if (connectionType == 0) connectionString = LocalConnection();
            else connectionString = ServerConnection();

            connection = new MySqlConnection(connectionString);
        }

        //open connection to database
        public bool OpenConnection()
        {            
            try
            {
                connection.Open();
                return true;
            }
            catch
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                //switch (ex.Number)
                //{
                //    case 0:
                //        break;

                //    case 1045:
                //        break;
                //}
                return false;
            }
        }

        //Close connection
        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                return false;
            }
        }

        private void QueryExecute(string inputQuery)
        {
            if (this.OpenConnection() == true)
            {
                //create mysql command
                MySqlCommand cmd = new MySqlCommand();
                //Assign the query using CommandText
                cmd.CommandText = inputQuery;
                //Assign the connection using Connection
                cmd.Connection = connection;

                //Execute query
                cmd.ExecuteNonQuery();

                //close connection
                this.CloseConnection();
            }
        }
        #endregion

        #region USERS
        // SELECT ALL USERS
        public List<User> User_SelectAll()
        {
            List<User> Users = new List<User>();
            string query = "SELECT * FROM users";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    User user = new User();
                    user.Id = (int)dataReader["id"];
                    user.Username = dataReader["name"].ToString();
                    user.Email = dataReader["email"].ToString();
                    user.Password_hash = dataReader["password_hash"].ToString();
                    user.Birthday = dataReader["birthday"].ToString();
                    user.Position = (int)dataReader["position"];
                    user.EmpID = (int)dataReader["empid"];
                    user.DirectManager = dataReader["directmanager"].ToString();
                    user.Api_key = dataReader["api_key"].ToString();
                    user.Status = (int)dataReader["status"];
                    user.Created_at = Convert.ToDateTime(dataReader["created_at"]);
                    switch (user.Position)
                    {
                        case 0:
                            user.PositionName = "Quản lí";
                            break;
                        case 1:
                            user.PositionName = "Nhân viên";
                            break;
                        case 2:
                            user.PositionName = "Kĩ thuật viên";
                            break;
                        default:
                            user.PositionName = "Nhân viên";
                            break;
                    }

                    switch (user.Status)
                    {
                        case 0:
                            user.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            user.StatusName = "Ngừng";
                            break;
                        default:
                            user.StatusName = "Đang hoạt động";
                            break;
                    }

                    Users.Add(user);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Users;
        }

        // SELECT ONE USER
        public User User_SelectOne(int id)
        {
            User user = new User();
            string query = "SELECT * FROM users WHERE id='" +id.ToString()+ "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    user.Id = (int)dataReader["id"];
                    user.Username = dataReader["name"].ToString();
                    user.Email = dataReader["email"].ToString();
                    user.Password_hash = dataReader["password_hash"].ToString();
                    user.Birthday = dataReader["birthday"].ToString();
                    user.Position = (int)dataReader["position"];
                    user.EmpID = (int)dataReader["empid"];
                    user.DirectManager = dataReader["directmanager"].ToString();
                    user.Api_key = dataReader["api_key"].ToString();
                    user.Status = (int)dataReader["status"];
                    switch (user.Status)
                    {
                        case 0:
                            user.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            user.StatusName = "Ngừng";
                            break;
                        default:
                            user.StatusName = "Đang hoạt động";
                            break;
                    }
                    switch (user.Position)
                    {
                        case 0:
                            user.PositionName = "Quản lí";
                            break;
                        case 1:
                            user.PositionName = "Nhân viên";
                            break;
                        case 2:
                            user.PositionName = "Kĩ thuật viên";
                            break;
                        default:
                            user.PositionName = "Nhân viên";
                            break;
                    }
                    user.Created_at = Convert.ToDateTime(dataReader["created_at"]);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return user;
        }

        // UPDATE 1 USER
        public void User_Update(User user, string email)
        {
            string query = "UPDATE users SET name = '" +user.Username + 
                                                  "', email = '" + user.Email.ToString() +
                                                  "', birthday = '" + user.Birthday.ToString() +
                                                  "', position = '" + user.Position.ToString() +
                                                  "', empid = '" + user.EmpID.ToString() +
                                                  "', directmanager = '" + user.DirectManager.ToString() +
                                                  "', api_key = '" + user.Api_key.ToString() +
                                                  "', status = '" + user.Status.ToString() +
                                                  "', created_at = '" + user.Created_at.ToString() +
                                                  "' WHERE email = '" + email + "' ";
            QueryExecute(query);
        }

        // CHANGE PASSWORD
        public void User_ChangePassword(string email, string pass_hash)
        {
            string query = "UPDATE users SET password_hash = '" + pass_hash +
                                                  "' WHERE email = '" + email + "' ";
            QueryExecute(query);
        }


        // INSERT 1 USER
        public void User_Insert(User user)
        {
            string query = "INSERT INTO users" +
                "(name, email, password_hash, birthday, position, empid, directmanager, api_key, status, created_at) VALUES (" +
                "'" + user.Username.ToString() + "'," +
                "'" + user.Email.ToString() + "'," +
                "'" + user.Password_hash.ToString() + "'," +
                "'" + user.Birthday.ToString() + "'," +
                "'" + user.Position.ToString() + "'," +
                "'" + user.EmpID.ToString() + "'," +
                "'" + user.DirectManager.ToString() + "'," +
                "'" + user.Api_key.ToString() + "'," +
                "'" + user.Status.ToString() + "'," +
                "'" + user.Created_at.ToString() + "')";
            QueryExecute(query);
        }
        #endregion

        #region CUSTOMER
        // SELECT ALL CUSTOMERS
        public List<Customer> Customer_SelectAll()
        {
            List<Customer> Customers = new List<Customer>();
            string query = "SELECT * FROM customer";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Customer cust = new Customer();
                    cust.CustomerID = dataReader["CustomerID"].ToString();
                    cust.Name = dataReader["Name"].ToString();
                    cust.Phone = dataReader["Phone"].ToString();
                    cust.Email = dataReader["Email"].ToString();
                    cust.Status = (int)dataReader["Status"];
                    cust.Created_at = Convert.ToDateTime(dataReader["created_at"]);
                    cust.Password_hash = dataReader["password_hash"].ToString();
                    switch (cust.Status)
                    {
                        case 0:
                            cust.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            cust.StatusName = "Ngừng";
                            break;
                        default:
                            cust.StatusName = "Đang hoạt động";
                            break;
                    }

                    Customers.Add(cust);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Customers;
        }

        // SELECT 1 CUSTOMER
        public Customer Customer_SelectOne(string custID)
        {
            Customer cust = new Customer();
            string query = "SELECT * FROM customer where CustomerID = '" + custID + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    cust.CustomerID = dataReader["CustomerID"].ToString();
                    cust.Name = dataReader["Name"].ToString();
                    cust.Phone = dataReader["Phone"].ToString();
                    cust.Email = dataReader["Email"].ToString();
                    cust.Status = (int)dataReader["Status"];
                    cust.Created_at = Convert.ToDateTime(dataReader["created_at"]);
                    cust.Password_hash = dataReader["password_hash"].ToString();
                    switch (cust.Status)
                    {
                        case 0:
                            cust.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            cust.StatusName = "Ngừng";
                            break;
                        default:
                            cust.StatusName = "Đang hoạt động";
                            break;
                    }
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return cust;
        }

        // SELECT ALL CUSTOMERIDS
        public List<string> Customer_SelectCustIDs()
        {
            List<string> custIds = new List<string>();
            string query = "SELECT CustomerID FROM customer";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    custIds.Add(dataReader["CustomerID"].ToString());
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return custIds;
        }


        // UPDATE 1 CUSTOMER
        public void Customer_Update(Customer cust, string CustID)
        {
            string query = "UPDATE customer SET Name = '" + cust.Name +
                                                  "', CustomerID = '" + cust.CustomerID.ToString() +
                                                  "', Phone = '" + cust.Phone.ToString() +
                                                  "', Email = '" + cust.Email.ToString() +
                                                  "', Status = '" + cust.Status.ToString() +
                                                  "', created_at = '" + cust.Created_at.ToString() +
                                                  "', password_hash = '" + cust.Password_hash.ToString() +
                                                  "' WHERE CustomerID = '" + CustID.ToString() + "' ";
            QueryExecute(query);
        }

        // INSERT 1 CUSTOMER 
        public void Customer_Insert(Customer cust)
        {
            string query = "INSERT INTO customer" +
                "(Name, CustomerID, Phone, Email, Status, created_at, password_hash) VALUES (" +
                "'" + cust.Name.ToString() + "'," +
                "'" + cust.CustomerID.ToString() + "'," +
                "'" + cust.Phone.ToString() + "'," +
                "'" + cust.Email.ToString() + "'," +
                "'" + cust.Status.ToString() + "'," +
                "'" + cust.Created_at.ToString() + "'," +
                "'" + cust.Password_hash.ToString() + "')";
            QueryExecute(query);
        }

        #endregion

        #region FARM
        // SELECT ALL CUSTOMERS
        public List<Farm> Farm_SelectAll()
        {
            List<Farm> Farms = new List<Farm>();
            string query = "SELECT * FROM farm";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Farm farm = new Farm();
                    farm.FarmID = dataReader["FarmID"].ToString();
                    farm.CustomerID = dataReader["customerid"].ToString();
                    farm.FarmName = dataReader["FarmName"].ToString();
                    farm.Location = dataReader["Location"].ToString();
                    farm.AreaSize = (double)dataReader["AreaSize"];
                    farm.NoCow = (int)dataReader["NoCow"];
                    farm.NoDevice = (int)dataReader["NoDevice"];
                    farm.Status = (int)dataReader["Status"];
                    switch (farm.Status)
                    {
                        case 0:
                            farm.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            farm.StatusName = "Ngừng";
                            break;
                        default:
                            farm.StatusName = "Đang hoạt động";
                            break;
                    }
                    Farms.Add(farm);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Farms;
        }

        // SELECT 1 FARM
        public Farm Farm_SelectOne(string farmID)
        {
            Farm farm = new Farm();
            string query = "SELECT * FROM farm where FarmID = '" + farmID + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    farm.FarmID = dataReader["FarmID"].ToString();
                    farm.CustomerID = dataReader["customerid"].ToString();
                    farm.FarmName = dataReader["FarmName"].ToString();
                    farm.Location = dataReader["Location"].ToString();
                    farm.AreaSize = (double)dataReader["AreaSize"];
                    farm.NoCow = (int)dataReader["NoCow"];
                    farm.NoDevice = (int)dataReader["NoDevice"];
                    farm.Status = (int)dataReader["Status"];
                    switch (farm.Status)
                    {
                        case 0:
                            farm.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            farm.StatusName = "Ngừng";
                            break;
                        default:
                            farm.StatusName = "Đang hoạt động";
                            break;
                    }
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return farm;
        }

        // SELECT ALL FARM IDs
        public List<string> Farm_SelectFarmIDs()
        {
            List<string> FarmIDs = new List<string>();
            string query = "SELECT FarmID FROM farm";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    FarmIDs.Add(dataReader["FarmID"].ToString());
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return FarmIDs;
        }

        // SELECT FARMS from CUSTOMER
        public List<Farm> Farm_SelectFarmsFromCustomer(string custid)
        {
            List<Farm> Farms = new List<Farm>();
            string query = "SELECT * FROM farm WHERE customerid = '" + custid + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Farm farm = new Farm();
                    farm.FarmID = dataReader["FarmID"].ToString();
                    farm.CustomerID = dataReader["customerid"].ToString();
                    farm.FarmName = dataReader["FarmName"].ToString();
                    farm.Location = dataReader["Location"].ToString();
                    farm.AreaSize = (double)dataReader["AreaSize"];
                    farm.NoDevice = (int)dataReader["NoDevice"];
                    farm.Status = (int)dataReader["Status"];
                    switch (farm.Status)
                    {
                        case 0:
                            farm.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            farm.StatusName = "Ngừng";
                            break;
                        default:
                            farm.StatusName = "Đang hoạt động";
                            break;
                    }
                    Farms.Add(farm);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Farms;
        }

        // UPDATE 1 FARM
        public void Farm_Update(Farm farm, string farmID)
        {
            string query = "UPDATE farm SET FarmID = '" + farm.FarmID +
                                                  "', customerid = '" + farm.CustomerID.ToString() +
                                                  "', FarmName = '" + farm.FarmName.ToString() +
                                                  "', Location = '" + farm.Location.ToString() +
                                                  "', AreaSize = '" + farm.AreaSize.ToString() +
                                                  "', Status = '" + farm.Status.ToString() +
                                                  "' WHERE FarmID = '" + farmID + "' ";
            QueryExecute(query);
        }

        // INSERT 1 FARM 
        public void Farm_Insert(Farm farm)
        {
            string query = "INSERT INTO farm" +
                "(FarmID, customerid, FarmName, Location, AreaSize, NoDevice, Status) VALUES (" +
                "'" + farm.FarmID.ToString() + "'," +
                "'" + farm.CustomerID.ToString() + "'," +
                "'" + farm.FarmName.ToString() + "'," +
                "'" + farm.Location.ToString() + "'," +
                "'" + farm.AreaSize.ToString() + "'," +
                "'" + farm.NoDevice.ToString() + "'," +
                "'" + farm.Status.ToString() + "')";
            QueryExecute(query);
        }

        public Dictionary<string,int> Farm_DeviceDict_As_farmid()
        {
            Dictionary<string, int> DeviceCount_As_FarmID = new Dictionary<string, int>();

            string query = "SELECT farmid, COUNT(*) FROM device " + 
                           "GROUP BY farmid";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DeviceCount_As_FarmID.Add(dataReader[0].ToString(), Convert.ToInt32(dataReader[1]));                
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return DeviceCount_As_FarmID;
        }

        public Dictionary<string, int> Farm_CowDict_As_farmid()
        {
            Dictionary<string, int> CowCount_As_FarmID = new Dictionary<string, int>();

            string query = "SELECT farmid, COUNT(*) FROM cow " +
                           "GROUP BY farmid";

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    CowCount_As_FarmID.Add(dataReader[0].ToString(), Convert.ToInt32(dataReader[1]));
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return CowCount_As_FarmID;
        }
        #endregion

        #region DEVICE
        // SELECT ALL DEVICES
        public List<Device> Device_SelectAll()
        {
            List<Device> Devices = new List<Device>();
            string query = "SELECT * FROM device where date(Created_at)";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Device dev = new Device();
                    dev.DeviceID = dataReader["DeviceID"].ToString();
                    dev.FarmID = dataReader["farmid"].ToString();
                    dev.HWVer = dataReader["HW_Ver"].ToString();
                    dev.SWVer = dataReader["SW_Ver"].ToString();
                    dev.DeviceName = dataReader["Device_Name"].ToString();
                    dev.Status = (int)dataReader["Status"];
                    dev.Created_at = Convert.ToDateTime(dataReader["created_at"]);
                    switch (dev.Status)
                    {
                        case 0:
                            dev.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            dev.StatusName = "Ngừng";
                            break;
                        default:
                            dev.StatusName = "Đang hoạt động";
                            break;
                    }

                    Devices.Add(dev);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Devices;
        }

        // SELECT DEVICES FROM 1 CUSTOMER
        public List<Device> Device_SelectFromFarmID(string farmId)
        {
            List<Device> Devices = new List<Device>();
            string query = "SELECT * FROM device WHERE farmid = '" + farmId + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Device dev = new Device();
                    dev.DeviceID = dataReader["DeviceID"].ToString();
                    dev.FarmID = dataReader["farmid"].ToString();
                    dev.HWVer = dataReader["HW_Ver"].ToString();
                    dev.SWVer = dataReader["SW_Ver"].ToString();
                    dev.DeviceName = dataReader["Device_Name"].ToString();
                    dev.Status = (int)dataReader["Status"];
                    dev.Created_at = Convert.ToDateTime(dataReader["created_at"]);
                    switch (dev.Status)
                    {
                        case 0:
                            dev.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            dev.StatusName = "Ngừng";
                            break;
                        default:
                            dev.StatusName = "Đang hoạt động";
                            break;
                    }

                    Devices.Add(dev);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Devices;
        }

        // SELECT 1 DEVICE
        public Device Device_SelectOne(string devID)
        {
            Device dev = new Device();
            string query = "SELECT * FROM device WHERE DeviceID = '" + devID + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    dev.DeviceID = dataReader["DeviceID"].ToString();
                    dev.FarmID = dataReader["farmid"].ToString();
                    dev.HWVer = dataReader["HW_Ver"].ToString();
                    dev.SWVer = dataReader["SW_Ver"].ToString();
                    dev.DeviceName = dataReader["Device_Name"].ToString();
                    dev.Status = (int)dataReader["Status"];
                    dev.Created_at = Convert.ToDateTime(dataReader["created_at"]);
                    switch (dev.Status)
                    {
                        case 0:
                            dev.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            dev.StatusName = "Ngừng";
                            break;
                        default:
                            dev.StatusName = "Đang hoạt động";
                            break;
                    }
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dev;
        }

        // UPDATE 1 DEVICE
        public void Device_Update(Device dev, string DevID)
        {
            string query = "UPDATE device SET DeviceID = '" + dev.DeviceID +
                                                  "', farmid = '" + dev.FarmID.ToString() +
                                                  "', HW_Ver = '" + dev.HWVer.ToString() +
                                                  "', SW_Ver = '" + dev.SWVer.ToString() +
                                                  "', Device_Name = '" + dev.DeviceName.ToString() +
                                                  "', Status = '" + dev.Status.ToString() +
                                                  "', created_at = '" + dev.Created_at.ToString() +
                                                  "' WHERE DeviceID = '" + DevID + "' ";
            QueryExecute(query);
        }


        // INSERT 1 DEVICE
        public void Device_Insert(Device dev)
        {
            string query = "INSERT INTO device" +
                "(DeviceID, farmid, HW_Ver, SW_Ver, Device_Name, Status, created_at) VALUES (" +
                "'" + dev.DeviceID.ToString() + "'," +
                "'" + dev.FarmID.ToString() + "'," +
                "'" + dev.HWVer.ToString() + "'," +
                "'" + dev.SWVer.ToString() + "'," +
                "'" + dev.DeviceName.ToString() + "'," +
                "'" + dev.Status.ToString() + "'," +
                "'" + dev.Created_at.ToString() + "')";
            QueryExecute(query);
        }
        #endregion

        #region COW
        // SELECT ALL COWS
        public List<Cow> Cow_SelectAll()
        {
            List<Cow> Cows = new List<Cow>();
            string query = "SELECT * FROM cow";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Cow cow = new Cow();
                    cow.CowID = dataReader["cowid"].ToString();
                    cow.FarmID = dataReader["farmid"].ToString();
                    cow.TagID_Left = dataReader["tag_id_left"].ToString();
                    cow.TagID_Right = dataReader["tag_id_right"].ToString();
                    cow.Date_In = Convert.ToDateTime(dataReader["date_in"]);
                    try
                    {
                        cow.Date_Out = Convert.ToDateTime(dataReader["date_out"]);
                    }
                    catch
                    {
                        cow.Date_Out = DateTime.MaxValue;
                    }
                    cow.Weight_Org = (double)dataReader["weight_org"];
                    cow.Breed_ID = dataReader["breed_id"].ToString();
                    cow.Day_Old = (int)dataReader["day_old_org"];
                    cow.Food_Digest = (double)dataReader["food_total"];
                    cow.Eff_Breeding = (double)dataReader["eff_cur"];
                    cow.Status = (int)dataReader["Status"];
                    switch (cow.Status)
                    {
                        case 0:
                            cow.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            cow.StatusName = "Ngừng";
                            break;
                        default:
                            cow.StatusName = "Đang hoạt động";
                            break;
                    }
                    Cows.Add(cow);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Cows;
        }

        // SELECT COWS FROM 1 DEVICE
        public List<Cow> Cow_SelectFromFarmID(string FarmID)
        {
            List<Cow> Cows = new List<Cow>();
            string query = "SELECT * FROM cow WHERE farmid = '" + FarmID + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Cow cow = new Cow();
                    cow.CowID = dataReader["cowid"].ToString();
                    cow.FarmID = dataReader["farmid"].ToString();
                    cow.TagID_Left = dataReader["tag_id_left"].ToString();
                    cow.TagID_Right = dataReader["tag_id_right"].ToString();
                    cow.Date_In = Convert.ToDateTime(dataReader["date_in"]);
                    try
                    {
                        cow.Date_Out = Convert.ToDateTime(dataReader["date_out"]);
                    }
                    catch
                    {
                        cow.Date_Out = DateTime.MaxValue;
                    }
                    cow.Weight_Org = (double)dataReader["weight_org"];
                    cow.Breed_ID = dataReader["breed_id"].ToString();
                    cow.Day_Old = (int)dataReader["day_old_org"];
                    cow.Food_Digest = (double)dataReader["food_total"];
                    cow.Eff_Breeding = (double)dataReader["eff_cur"];
                    cow.Status = (int)dataReader["Status"];
                    switch (cow.Status)
                    {
                        case 0:
                            cow.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            cow.StatusName = "Ngừng";
                            break;
                        default:
                            cow.StatusName = "Đang hoạt động";
                            break;
                    }
                    Cows.Add(cow);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return Cows;
        }

        // SELECT 1 COW
        public Cow Cow_SelectOne(string CowID)
        {
            Cow cow = new Cow();
            string query = "SELECT * FROM cow WHERE cowid = '" + CowID + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    cow.CowID = dataReader["cowid"].ToString();
                    cow.FarmID = dataReader["farmid"].ToString();
                    cow.TagID_Left = dataReader["tag_id_left"].ToString();
                    cow.TagID_Right = dataReader["tag_id_right"].ToString();
                    cow.Date_In = Convert.ToDateTime(dataReader["date_in"]);
                    try
                    {
                        cow.Date_Out = Convert.ToDateTime(dataReader["date_out"]);
                    }
                    catch
                    {
                        cow.Date_Out = DateTime.MaxValue;
                    }
                    cow.Weight_Org = (double)dataReader["weight_org"];
                    cow.Breed_ID = dataReader["breed_id"].ToString();
                    cow.Day_Old = (int)dataReader["day_old_org"];
                    cow.Food_Digest = (double)dataReader["food_total"];
                    cow.Eff_Breeding = (double)dataReader["eff_cur"];
                    cow.Status = (int)dataReader["Status"];
                    switch (cow.Status)
                    {
                        case 0:
                            cow.StatusName = "Đang hoạt động";
                            break;
                        case 1:
                            cow.StatusName = "Ngừng";
                            break;
                        default:
                            cow.StatusName = "Đang hoạt động";
                            break;
                    }
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return cow;
        }

        // UPDATE 1 COW
        public void Cow_Update(Cow cow, string CowID)
        {
            string query = "UPDATE cow SET cowid = '" + cow.CowID +
                                                  "', farmid = '" + cow.FarmID.ToString() +
                                                  "', tag_id_left = '" + cow.TagID_Left.ToString() +
                                                  "', tag_id_right = '" + cow.TagID_Right.ToString() +
                                                  "', date_in = '" + cow.Date_In.ToString() +
                                                  "', date_out = '" + cow.Date_Out.ToString() +
                                                  "', weight_org = '" + cow.Weight_Org.ToString() +
                                                  "', breed_id = '" + cow.Breed_ID.ToString() +
                                                  "', day_old_org = '" + cow.Day_Old.ToString() +
                                                  "', status = '" + cow.Status.ToString() +
                                                  "' WHERE cowid = '" + CowID + "' ";
            QueryExecute(query);
        }


        // INSERT 1 COW
        public void Cow_Insert(Cow cow)
        {
            string query = "INSERT INTO cow" +
                "(cowid, farmid, tag_id_left, tag_id_right, date_in, weight_org, breed_id, day_old_org, status) VALUES (" +
                "'" + cow.CowID.ToString() + "'," +
                "'" + cow.FarmID.ToString() + "'," +
                "'" + cow.TagID_Left.ToString() + "'," +
                "'" + cow.TagID_Right.ToString() + "'," +
                "'" + cow.Date_In.ToString() + "'," +
                "'" + cow.Weight_Org.ToString() + "'," +
                "'" + cow.Breed_ID.ToString() + "'," +
                "'" + cow.Day_Old.ToString() + "'," +
                "'" + cow.Status.ToString() + "')";
            QueryExecute(query);
        }
        #endregion

        #region DAILY DEVICE
        // SELECT ALL DAILY DEVICES
        public Dictionary<string, List<DailyDevice>> DailyDevice_SelectAll()
        {
            Dictionary<string, List<DailyDevice>> dailyDevs = new Dictionary<string, List<DailyDevice>>();
            string query = "SELECT * FROM daily_device";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DailyDevice dd = new DailyDevice();
                    dd.DeviceID = dataReader["deviceid"].ToString();
                    dd.Date = dataReader["date_update"].ToString();
                    dd.Temperature = dataReader["Temperature"].ToString();
                    dd.Humidity = dataReader["Humidity"].ToString();
                    if (dailyDevs.ContainsKey(dd.DeviceID))
                    {
                        dailyDevs[dd.DeviceID].Add(dd);
                    }
                    else
                    {
                        List<DailyDevice> list = new List<DailyDevice>();
                        dailyDevs.Add(dd.DeviceID, list);
                    }
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dailyDevs;
        }

        // SELECT DAILY DEVICES FROM 1 DEVICE
        public List<DailyDevice> DailyDevice_SelectByDevice(string DevId)
        {
            List<DailyDevice> dailyDevs = new List<DailyDevice>();
            string query = "SELECT * FROM daily_device where deviceid = '" + DevId + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DailyDevice dd = new DailyDevice();
                    dd.DeviceID = dataReader["deviceid"].ToString();
                    dd.Date = dataReader["date_update"].ToString();
                    dd.Temperature = dataReader["Temperature"].ToString();
                    dd.Humidity = dataReader["Humidity"].ToString();
                    dailyDevs.Add(dd);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dailyDevs;
        }

        // SELECT DAILY DEVICES FROM 1 DATE
        public List<DailyDevice> DailyDevice_SelectByDate(string Date)
        {
            List<DailyDevice> dailyDevs = new List<DailyDevice>();
            string query = "SELECT * FROM daily_device where DateUpdate = '" + Date + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DailyDevice dd = new DailyDevice();
                    dd.DeviceID = dataReader["deviceid"].ToString();
                    dd.Date = dataReader["date_update"].ToString();
                    dd.Temperature = dataReader["Temperature"].ToString();
                    dd.Humidity = dataReader["Humidity"].ToString();
                    dailyDevs.Add(dd);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dailyDevs;
        }

        // SELECT 1 DAILY DEVICE ROW 
        public DailyDevice DailyDevice_SelectOne(string DevId, string Date)
        {
            string query = "SELECT * FROM daily_device WHERE DateUpdate = '" + Date + "' " +
                                                      "AND deviceid = '" + DevId + "'";
            DailyDevice dd = new DailyDevice();

            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    dd.DeviceID = dataReader["deviceid"].ToString();
                    dd.Date = dataReader["date_update"].ToString();
                    dd.Temperature = dataReader["Temperature"].ToString();
                    dd.Humidity = dataReader["Humidity"].ToString();
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dd;
        }
        #endregion

        #region DAILY COW
        // SELECT ALL DAILY COW
        public Dictionary<string, List<DailyCow>> DailyCow_SelectAll()
        {
            Dictionary<string, List<DailyCow>> dailyCows = new Dictionary<string, List<DailyCow>>();
            string query = "SELECT * FROM daily_cow";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DailyCow dc = new DailyCow();
                    DateTime date = Convert.ToDateTime(dataReader["date_update"].ToString());
                    dc.CowID = dataReader["cowid"].ToString();
                    dc.Date = date.ToString("dd-MM-yy");
                    dc.Weight = (double)dataReader["weight_cur"];
                    if (dailyCows.ContainsKey(dc.CowID))
                    {
                        dailyCows[dc.CowID].Add(dc);
                    }
                    else
                    {
                        List<DailyCow> list = new List<DailyCow>();
                        list.Add(dc);
                        dailyCows.Add(dc.CowID, list);
                    }
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dailyCows;
        }

        // SELECT DAILY COW FROM 1 COW
        public List<DailyCow> DailyCow_SelectByCowId(string CowId)
        {
            List<DailyCow> dailyCows = new List<DailyCow>();
            string query = "SELECT * FROM daily_cow WHERE cowid = '"+ CowId + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DailyCow dc = new DailyCow();
                    dc.CowID = dataReader["cowid"].ToString();
                    dc.Date = dataReader["date_update"].ToString();
                    dc.Weight = (double)dataReader["weight_cur"];
                    dailyCows.Add(dc);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dailyCows;
        }

        // SELECT DAILY COW FROM 1 DATE
        public List<DailyCow> DailyCow_SelectByDate(string Date)
        {
            List<DailyCow> dailyCows = new List<DailyCow>();
            string query = "SELECT * FROM daily_cow WHERE date = '" + Date + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DailyCow dc = new DailyCow();
                    dc.CowID = dataReader["cowid"].ToString();
                    dc.Date = dataReader["date_update"].ToString();
                    dc.Weight = (double)dataReader["weight_cur"];
                    dailyCows.Add(dc);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dailyCows;
        }

        // SELECT 1 DAILY COW
        public DailyCow DailyCow_SelectOne(string CowId, string Date)
        {
            DailyCow dc = new DailyCow();
            string query = "SELECT * FROM daily_cow WHERE date = '" + Date + "'" +
                                                    "AND cowid = '" + CowId + "'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    dc.CowID = dataReader["cowid"].ToString();
                    dc.Date = dataReader["date_update"].ToString();
                    dc.Weight = (double)dataReader["weight_cur"];
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return dc;
        }
        #endregion

        #region COST
        public List<Cost> Cost_SelectAll()
        {
            List<Cost> costs = new List<Cost>();
            string query = "SELECT * FROM cost";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    DateTime date;
                    Cost c = new Cost();
                    c.id = (int)dataReader["id"];
                    c.Cost_Meat = (double)dataReader["beef_val_std"];
                    c.Cost_Bogaca = (double)dataReader["bogaca_val_std"];
                    date = Convert.ToDateTime(dataReader["date_update"]);
                    c.Update_Date = date.ToString("dd/MM/yyyy");
                    c.Breed_ID = dataReader["breed_id"].ToString();
                    costs.Add(c);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return costs;
        }

        public void Cost_Update(Cost cost)
        {
            string query = "UPDATE cost SET eff_std = '" + cost.Stand_Eff_Ratio.ToString() +
                                                  "', breed_id = '" + cost.Breed_ID.ToString() +
                                                  "', beef_val_std = '" + cost.Cost_Meat.ToString() +
                                                  "', bogaca_val_std = '" + cost.Cost_Bogaca.ToString() +
                                                  "', date_update = '" + cost.Update_Date.ToString() +
                                                  "' WHERE id = '" + cost.id + "' ";
            QueryExecute(query);
        }

        public void Cost_Add(Cost cost)
        {
            string query = "INSERT INTO cost" +
                "(eff_std, beef_val_std, bogaca_val_std, breed_id, date_update) VALUES (" +
                "'" + cost.Stand_Eff_Ratio.ToString() + "', " +
                "'" + cost.Cost_Meat.ToString() + "', " +
                "'" + cost.Cost_Bogaca.ToString() + "', " +
                "'default', " +
                "'" + cost.Update_Date.ToString() + "')";
            QueryExecute(query);
        }
        #endregion

        #region STANDARD
        public List<Standard> Standard_SelectAll()
        {
            List<Standard> stands = new List<Standard>();
            string query = "SELECT * FROM cow_standard_weightline WHERE breed_id = 'default'";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    Standard st = new Standard();
                    st.Breed_ID = dataReader["breed_id"].ToString();
                    st.day_old = (int)dataReader["day_old"];
                    st.weight_std = (double)dataReader["weight_std"];
                    st.weight_gain = (double)dataReader["weight_gain"];
                    stands.Add(st);
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return stands;
        }

        public void Standard_Update(Standard std)
        {
            string query = "UPDATE standard_cow SET day_old = '" + std.day_old +
                                                  "', breed_id = '" + std.Breed_ID.ToString() +
                                                  "', weight_std = '" + std.weight_std.ToString() +
                                                  "', weight_gain = '" + std.weight_gain.ToString() +
                                                  "' WHERE id = '" + std.id + "' ";
            QueryExecute(query);
        }

        public void Standard_Add(Standard std)
        {
            string query = "INSERT INTO standard_cow" +
                "(breed_id, day_old, weight_std, weight_gain) VALUES (" +
                "'default', " +
                "'" + std.day_old.ToString() + "'," +
                "'" + std.weight_std.ToString() + "'," +
                "'" + std.weight_gain.ToString() + "')";
            QueryExecute(query);
        }
        #endregion

        #region Threshold
        public Threshold Threshold_SelectNew()
        {
            Threshold thres = new Threshold();
            string query = "SELECT * FROM threshold " +
                            "ORDER BY id DESC " +
                            "LIMIT 1";
            if (this.OpenConnection() == true)
            {
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    thres.id = (int)dataReader["id"];
                    thres.time_update = Convert.ToDateTime(dataReader["time_update"]);
                    thres.weight_std_div = (double)dataReader["weight_std_div"];
                    thres.weight_gain_div = (double)dataReader["weight_gain_div"];
                    thres.bogaca_gain_eff_div = (double)dataReader["bogaca_gain_eff_div"];
                    thres.cow_temp_norm = (double)dataReader["cow_temp_norm"];
                    thres.cow_temp_div = (double)dataReader["cow_temp_div"];
                    thres.dev_temp_norm = (double)dataReader["dev_temp_norm"];
                    thres.dev_temp_div = (double)dataReader["dev_temp_div"];
                    thres.dev_humid_norm = (double)dataReader["dev_humid_norm"];
                    thres.dev_humid_div = (double)dataReader["dev_humid_div"];
                }
                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
            }
            return thres;
        }

        public void Threshold_Add(Threshold thres)
        {
            string query = "INSERT INTO threshold" +
                "(time_update, weight_std_div, weight_gain_div, bogaca_gain_eff_div, cow_temp_norm, cow_temp_div, dev_temp_norm, dev_temp_div, dev_humid_norm, dev_humid_div) VALUES (" +
                "'" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "'," +
                "'" + thres.weight_std_div.ToString() + "'," +
                "'" + thres.weight_gain_div.ToString() + "'," +
                "'" + thres.bogaca_gain_eff_div.ToString() + "'," +
                "'" + thres.cow_temp_norm.ToString() + "'," +
                "'" + thres.cow_temp_div.ToString() + "'," +
                "'" + thres.dev_temp_norm.ToString() + "'," +
                "'" + thres.dev_temp_div.ToString() + "'," +
                "'" + thres.dev_humid_norm.ToString() + "'," +
                "'" + thres.dev_humid_div.ToString() + "')";
            QueryExecute(query);
        }

        #endregion

    }
}
