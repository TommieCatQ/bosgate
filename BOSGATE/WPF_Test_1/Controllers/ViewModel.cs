﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSGATE
{
    public class ViewModel
    {
        private DatabaseController svDB = new DatabaseController(1);
        public List<KeyValuePair<int,double>> Weight_Std
        {
            get
            {
                List<KeyValuePair<int, double>> points = new List<KeyValuePair<int, double>>();
                List<Standard> stds = svDB.Standard_SelectAll();
                foreach (var s in stds)
                {
                    points.Add(new KeyValuePair<int, double>(s.day_old, s.weight_std));
                }
                return points;
            }
        }
        public List<KeyValuePair<int, double>> Weight_Gain
        {
            get
            {
                List<KeyValuePair<int, double>> points = new List<KeyValuePair<int, double>>();
                List<Standard> stds = svDB.Standard_SelectAll();
                foreach (var s in stds)
                {
                    points.Add(new KeyValuePair<int, double>(s.day_old, s.weight_gain));
                }
                return points;
            }
        }

        public List<KeyValuePair<string, double>> Price_Beef
        {
            get
            {
                List<KeyValuePair<string, double>> points = new List<KeyValuePair<string, double>>();
                List<Cost> cost = svDB.Cost_SelectAll();
                foreach (var c in cost)
                {
                    points.Add(new KeyValuePair<string, double>(c.Update_Date, c.Cost_Meat));
                }
                return points;
            }
        }

        public List<KeyValuePair<string, double>> Price_Bogaca
        {
            get
            {
                List<KeyValuePair<string, double>> points = new List<KeyValuePair<string, double>>();
                List<Cost> cost = svDB.Cost_SelectAll();
                foreach (var c in cost)
                {
                    points.Add(new KeyValuePair<string, double>(c.Update_Date, c.Cost_Bogaca));
                }
                return points;
            }
        }
    }
}
