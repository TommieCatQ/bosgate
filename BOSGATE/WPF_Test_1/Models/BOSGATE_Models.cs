﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSGATE
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password_hash { get; set; }
        public string Birthday { get; set; }
        public int Position { get; set; }
        public string PositionName { get; set; }
        public int EmpID { get; set; }
        public string DirectManager { get; set; }
        public string Api_key { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public DateTime Created_at { get; set; }

        public User()
        {

        }
        
        public User(int id, string username, string email, string birthday, int empid, int position, DateTime createdat)
        {
            Id = id;
            Username = username;
            Email = email;
            Birthday = birthday;
            Position = position;
            EmpID = empid;
            Created_at = createdat;

            switch (Position)
            {
                case 0:
                    PositionName = "Quản lí";
                    break;
                case 1:
                    PositionName = "Nhân viên";
                    break;
                case 2:
                    PositionName = "Kĩ thuật viên";
                    break;
                default:
                    PositionName = "Nhân Viên";
                    break;
            }       
        }
    }
    public class Root_User
    {
        public bool error { get; set; }
        public List<User> users { get; set; }
    }

    public class Customer
    {
        public int ID { get; set; }
        public string CustomerID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public DateTime Created_at { get; set; }
        public string Password_hash { get; set; }

        public Customer()
        {

        }
        public Customer(string custid, string name, string email, string phone, string passhash)
        {
            CustomerID = custid;
            Name = name;
            Email = email;
            Phone = phone;
            Status = 0;
            Created_at = DateTime.Today;
        }
    }
    public class Root_Customer
    {
        public bool error { get; set; }
        public List<Customer> customers { get; set; }
    }

    public class Farm
    {
        public int Id { get; set; }
        public string FarmID { get; set; }
        public string CustomerID { get; set; }
        public string FarmName { get; set; }
        public string Location { get; set; }
        public double AreaSize { get; set; }
        public int NoDevice { get; set; }
        public int NoCow { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
    }

    public class Device
    {
        public int Id { get; set; }
        public string DeviceID { get; set; }
        public string FarmID { get; set; }
        public string HWVer { get; set; }
        public string SWVer { get; set; }
        public string Address { get; set; }
        public string DeviceName { get; set; }
        public DateTime Created_at { get; set; }
        public string Sim_SN { get; set; }
        public string Sim_ExpiryDate { get; set; }
        public string Sim_Balance { get; set; }
        public string BatteryStatus { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }

        public Device() { }

        public Device(string devID, string farmid, string hwver, string swver, string address, string name)
        {
            DeviceID = devID;
            FarmID = farmid;
            HWVer = hwver;
            SWVer = swver;
            Address = address;
            DeviceName = name;
        }
    }
    public class Root_Device
    {
        public bool error { get; set; }
        public List<Device> devices { get; set; }
    }

    public class DailyDevice
    {
        public string DeviceID { get; set; }
        public string Date { get; set; }
        public string Temperature { get; set; }
        public string Humidity { get; set; }
        public double Food_Day { get; set; }
    }
    public class Root_DailyDevice
    {
        public bool error { get; set; }
        public List<DailyDevice> dailydevices { get; set; }
    }


    public class Cow
    {
        public int Id { get; set; }
        public string CowID { get; set; }
        public string FarmID { get; set; }
        public string TagID_Left { get; set; }
        public string TagID_Right { get; set; }
        public DateTime Date_In { get; set; }
        public DateTime Date_Out { get; set; }
        public double Weight_Org { get; set; }
        public string Breed_ID { get; set; }
        public int Day_Old { get; set; }
        public double Food_Digest { get; set; }
        public double Eff_Breeding { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
    }
    public class Root_Cow
    {
        public bool error { get; set; }
        public List<Device> cows { get; set; }
    }

    public class DailyCow
    {
        public string CowID { get; set; }
        public string Date { get; set; }
        public double Weight { get; set; }
    }
    public class Root_DailyCow
    {
        public bool error { get; set; }
        public List<DailyCow> dailycow { get; set; }
    }


    public class Cost
    {
        public int id { get; set; }
        public string Update_Date { get; set; }
        public string Breed_ID { get; set; }
        public double Stand_Eff_Ratio { get; set; }
        public double Cost_Bogaca { get; set; }
        public double Cost_Meat { get; set; }
    }
    public class Root_Cost
    {
        public bool error { get; set; }
        public List<Cost> costs { get; set; }
    }


    public class Standard
    {
        public int id { get; set; }
        public string Breed_ID { get; set; }
        public int day_old { get; set; }
        public double weight_gain { get; set; }
        public double weight_std { get; set; }

        public Standard()
        {

        }
    }

    public class Threshold
    {
        public int id { get; set; }
        public DateTime time_update { get; set; }
        public double weight_std_div { get; set; }
        public double weight_gain_div { get; set; }
        public double bogaca_gain_eff_div { get; set; }
        public double cow_temp_norm { get; set; }
        public double cow_temp_div { get; set; }
        public double dev_temp_norm { get; set; }
        public double dev_temp_div { get; set; }
        public double dev_humid_norm { get; set; }
        public double dev_humid_div { get; set; }
    }
}
