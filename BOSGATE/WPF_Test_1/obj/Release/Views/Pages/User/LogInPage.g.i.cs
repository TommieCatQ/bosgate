﻿#pragma checksum "..\..\..\..\..\Views\Pages\User\LogInPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "C43D86E6A96B3132D18141A804F9E1F8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using BOSGATE;
using BOSGATE.Properties;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace BOSGATE {
    
    
    /// <summary>
    /// LogInPage
    /// </summary>
    public partial class LogInPage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 1 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BOSGATE.LogInPage loginPage;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Grid_General;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Menu menu;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tB_System;
        
        #line default
        #line hidden
        
        
        #line 125 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tB_LogIn;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tB_User;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tB_Pass;
        
        #line default
        #line hidden
        
        
        #line 132 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tBox_User;
        
        #line default
        #line hidden
        
        
        #line 134 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox tBox_Pass;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox checkBox;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button but_Start;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button but_Register;
        
        #line default
        #line hidden
        
        
        #line 145 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Text_Warning;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/BOSGATE;component/views/pages/user/loginpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.loginPage = ((BOSGATE.LogInPage)(target));
            
            #line 12 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
            this.loginPage.Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Grid_General = ((System.Windows.Controls.Grid)(target));
            
            #line 121 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
            this.Grid_General.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.Grid_General_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.menu = ((System.Windows.Controls.Menu)(target));
            return;
            case 4:
            this.tB_System = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.tB_LogIn = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.tB_User = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.tB_Pass = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.tBox_User = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.tBox_Pass = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 10:
            this.checkBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.but_Start = ((System.Windows.Controls.Button)(target));
            
            #line 142 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
            this.but_Start.Click += new System.Windows.RoutedEventHandler(this.but_Start_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.but_Register = ((System.Windows.Controls.Button)(target));
            
            #line 143 "..\..\..\..\..\Views\Pages\User\LogInPage.xaml"
            this.but_Register.Click += new System.Windows.RoutedEventHandler(this.but_Register_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.Text_Warning = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

