﻿namespace BOSGATE
{
    partial class AddCost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.but_Add = new System.Windows.Forms.Button();
            this.tB_CostBogaca = new System.Windows.Forms.TextBox();
            this.tB_CostBeef = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tB_EffRatio = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // but_Add
            // 
            this.but_Add.Location = new System.Drawing.Point(78, 175);
            this.but_Add.Name = "but_Add";
            this.but_Add.Size = new System.Drawing.Size(114, 38);
            this.but_Add.TabIndex = 16;
            this.but_Add.Text = "Thêm";
            this.but_Add.UseVisualStyleBackColor = true;
            this.but_Add.Click += new System.EventHandler(this.but_Add_Click);
            // 
            // tB_CostBogaca
            // 
            this.tB_CostBogaca.Location = new System.Drawing.Point(157, 84);
            this.tB_CostBogaca.Name = "tB_CostBogaca";
            this.tB_CostBogaca.Size = new System.Drawing.Size(100, 22);
            this.tB_CostBogaca.TabIndex = 15;
            // 
            // tB_CostBeef
            // 
            this.tB_CostBeef.Location = new System.Drawing.Point(157, 41);
            this.tB_CostBeef.Name = "tB_CostBeef";
            this.tB_CostBeef.Size = new System.Drawing.Size(100, 22);
            this.tB_CostBeef.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Giá 1kg Bogaca";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 17);
            this.label2.TabIndex = 12;
            this.label2.Text = "Giá 1kg thịt hơi";
            // 
            // tB_EffRatio
            // 
            this.tB_EffRatio.Location = new System.Drawing.Point(157, 123);
            this.tB_EffRatio.Name = "tB_EffRatio";
            this.tB_EffRatio.Size = new System.Drawing.Size(100, 22);
            this.tB_EffRatio.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 123);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 17);
            this.label1.TabIndex = 17;
            this.label1.Text = "Tỉ lệ Bogaca/Hơi";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // AddCost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.tB_EffRatio);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.but_Add);
            this.Controls.Add(this.tB_CostBogaca);
            this.Controls.Add(this.tB_CostBeef);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "AddCost";
            this.Text = "Cập nhật giá cả";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button but_Add;
        private System.Windows.Forms.TextBox tB_CostBogaca;
        private System.Windows.Forms.TextBox tB_CostBeef;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tB_EffRatio;
        private System.Windows.Forms.Label label1;
    }
}