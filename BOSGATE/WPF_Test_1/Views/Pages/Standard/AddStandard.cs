﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace BOSGATE
{
    public partial class AddStandard : Form
    {
        Standard std = new Standard();
        DatabaseController svDB = new DatabaseController(1);

        public AddStandard()
        {
            InitializeComponent();
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void but_Add_Click(object sender, EventArgs e)
        {
            std.day_old = Convert.ToInt16(tB_day.Text);
            std.weight_std = Convert.ToDouble(tB_weight_std.Text);
            std.weight_gain = Convert.ToDouble(tB_weight_gain.Text);
            try
            {
                svDB.Standard_Add(std);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Ngày tuổi không được trùng", "Nhắc nhở", MessageBoxButton.OK);
            }
            this.Close();
        }
    }
}
