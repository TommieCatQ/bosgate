﻿namespace BOSGATE
{
    partial class AddStandard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tB_day = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tB_weight_std = new System.Windows.Forms.TextBox();
            this.tB_weight_gain = new System.Windows.Forms.TextBox();
            this.but_Add = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tB_day
            // 
            this.tB_day.Location = new System.Drawing.Point(151, 57);
            this.tB_day.Name = "tB_day";
            this.tB_day.Size = new System.Drawing.Size(100, 22);
            this.tB_day.TabIndex = 0;
            this.tB_day.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ngày tuổi";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(34, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Cân nặng chuẩn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(34, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tăng cân chuẩn";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tB_weight_std
            // 
            this.tB_weight_std.Location = new System.Drawing.Point(151, 98);
            this.tB_weight_std.Name = "tB_weight_std";
            this.tB_weight_std.Size = new System.Drawing.Size(100, 22);
            this.tB_weight_std.TabIndex = 7;
            this.tB_weight_std.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // tB_weight_gain
            // 
            this.tB_weight_gain.Location = new System.Drawing.Point(151, 141);
            this.tB_weight_gain.Name = "tB_weight_gain";
            this.tB_weight_gain.Size = new System.Drawing.Size(100, 22);
            this.tB_weight_gain.TabIndex = 8;
            this.tB_weight_gain.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // but_Add
            // 
            this.but_Add.Location = new System.Drawing.Point(79, 193);
            this.but_Add.Name = "but_Add";
            this.but_Add.Size = new System.Drawing.Size(114, 38);
            this.but_Add.TabIndex = 9;
            this.but_Add.Text = "Thêm";
            this.but_Add.UseVisualStyleBackColor = true;
            this.but_Add.Click += new System.EventHandler(this.but_Add_Click);
            // 
            // AddStandard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.but_Add);
            this.Controls.Add(this.tB_weight_gain);
            this.Controls.Add(this.tB_weight_std);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tB_day);
            this.Name = "AddStandard";
            this.Text = "Thêm tiêu chuẩn";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tB_day;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tB_weight_std;
        private System.Windows.Forms.TextBox tB_weight_gain;
        private System.Windows.Forms.Button but_Add;
    }
}