﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for PageStandard.xaml
    /// </summary>
    public partial class PageStandard : Page
    {

        // Standard variables
        AddStandard addstd;
        List<Standard> Stds = new List<Standard>();
        Dictionary<int, Standard> changed_Stds = new Dictionary<int, Standard>();

        // Cost variables
        AddCost addcost;
        List<Cost> Costs = new List<Cost>();
        Dictionary<int, Cost> changed_Costs = new Dictionary<int, Cost>();

        DatabaseController svDB = new DatabaseController(1);
        private const string SELECT_SQL = "SELECT * FROM standard";


        private void InitStandard()
        {
            Stds = svDB.Standard_SelectAll();
            DataContext = new ViewModel();
            dgStd.ItemsSource = Stds;
        }

        private void InitCost()
        {

            Costs = svDB.Cost_SelectAll();
            DataContext = new ViewModel();
            dgCost.ItemsSource = Costs;
        }

        public PageStandard()
        {
            InitializeComponent();

            // Threads init

            // Init data
            InitStandard();
            InitCost();
            InitThreshold();
        }

        #region Cow Weight Standard
        private void Addstd_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            InitStandard();
        }

        private void LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                if (e.Row.GetIndex() % 2 == 0)
                {
                    e.Row.Background = new SolidColorBrush(Colors.White);
                    e.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    e.Row.Background = new SolidColorBrush(Colors.DimGray);
                    e.Row.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }

        }

        private void dgStd_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            Standard SelectedStand = (Standard)dgStd.SelectedItem;
            if (changed_Stds.ContainsKey(SelectedStand.id))
            {
                changed_Stds[SelectedStand.id] = SelectedStand;
            }
            else
            {
                changed_Stds.Add(SelectedStand.id, SelectedStand);

            }
        }

        private void but_Edit_Click(object sender, RoutedEventArgs e)
        {
            foreach (var s in changed_Stds)
            {
                svDB.Standard_Update(s.Value);
            }
            changed_Stds = new Dictionary<int, Standard>();
            this.DataContext = new ViewModel();
        }

        private void but_Add_Click(object sender, RoutedEventArgs e)
        {
            addstd = new AddStandard();
            addstd.FormClosed += Addstd_FormClosed;
            addstd.ShowDialog();
        }

        private void but_Undo_Click(object sender, RoutedEventArgs e)
        {
            InitStandard();
        }

        private void TBox_StandSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgStd.ItemsSource);
            cv.Filter = o =>
            {
                /* change to get data row value */
                Standard p = o as Standard;
                return (p.day_old.ToString().ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }
        #endregion

        #region Cost
        private void TBox_CostSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgStd.ItemsSource);
            cv.Filter = o =>
            {
                /* change to get data row value */
                Cost p = o as Cost;
                return (p.Update_Date.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }

        private void but_CostAdd_Click(object sender, RoutedEventArgs e)
        {
            addcost = new AddCost();
            addcost.FormClosed += addcost_FormClosed;
            addcost.ShowDialog();
        }

        void addcost_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            InitCost();
        }
        #endregion

        #region Threshold
        private void InitThreshold()
        {
            // Threshold variables
            Threshold thres = new Threshold();
            thres = svDB.Threshold_SelectNew();
            tBox_weight_stan_div.Text = thres.weight_std_div.ToString();
            tBox_weight_gain_div.Text = thres.weight_gain_div.ToString();
            tBox_bogaca_gain_eff_div.Text = thres.bogaca_gain_eff_div.ToString();
            tBox_cow_temp_norm.Text = thres.cow_temp_norm.ToString();
            tBox_cow_temp_div.Text = thres.cow_temp_div.ToString();

            tBox_dev_temp_norm.Text = thres.dev_temp_norm.ToString();
            tBox_dev_temp_div.Text = thres.dev_temp_div.ToString();
            tBox_dev_humid_norm.Text = thres.dev_humid_norm.ToString();
            tBox_dev_humid_div.Text = thres.dev_humid_div.ToString();
        }
        private void but_Save_Click(object sender, RoutedEventArgs e)
        {
            Threshold NewThres = new Threshold();

            NewThres.weight_std_div = Convert.ToDouble(tBox_weight_stan_div.Text);
            NewThres.weight_gain_div = Convert.ToDouble(tBox_weight_gain_div.Text);
            NewThres.bogaca_gain_eff_div = Convert.ToDouble(tBox_bogaca_gain_eff_div.Text);
            NewThres.cow_temp_norm = Convert.ToDouble(tBox_cow_temp_norm.Text);
            NewThres.cow_temp_div = Convert.ToDouble(tBox_cow_temp_div.Text);

            NewThres.dev_temp_norm = Convert.ToDouble(tBox_dev_temp_norm.Text);
            NewThres.dev_temp_div = Convert.ToDouble(tBox_dev_temp_div.Text);
            NewThres.dev_humid_norm = Convert.ToDouble(tBox_dev_humid_norm.Text);
            NewThres.dev_humid_div = Convert.ToDouble(tBox_dev_humid_div.Text);

            if ((tBox_weight_stan_div.Text == "") ||
                (tBox_weight_gain_div.Text == "") ||
                (tBox_bogaca_gain_eff_div.Text == "") ||
                (tBox_cow_temp_norm.Text == "") ||
                (tBox_cow_temp_div.Text == "") ||
                (tBox_dev_temp_norm.Text == "") ||
                (tBox_dev_temp_div.Text == "") ||
                (tBox_dev_humid_norm.Text == "") ||
                (tBox_dev_humid_div.Text == "")
                )
            {
                Warning.Foreground = new SolidColorBrush(Colors.Red);
                Warning.Text = "Không được để trường trống";
            }
            else
            {
                svDB.Threshold_Add(NewThres);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã cập nhật thành công";
            }
        }

        #endregion

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }


    }
}
