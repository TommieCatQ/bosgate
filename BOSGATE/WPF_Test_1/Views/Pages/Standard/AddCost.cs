﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace BOSGATE
{
    public partial class AddCost : Form
    {
        Cost cost = new Cost();
        DatabaseController svDB = new DatabaseController(1);

        public AddCost()
        {
            InitializeComponent();
        }

        private void but_Add_Click(object sender, EventArgs e)
        {
            cost.Cost_Meat = Convert.ToDouble(tB_CostBeef.Text);
            cost.Cost_Bogaca = Convert.ToDouble(tB_CostBogaca.Text);
            cost.Stand_Eff_Ratio = Convert.ToDouble(tB_EffRatio.Text);
            cost.Update_Date = DateTime.Today.ToShortDateString();
            try
            {
                svDB.Cost_Add(cost);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Hôm nay đã nhập giá rồi", "Nhắc nhở", MessageBoxButton.OK);
            }
            this.Close();

        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

    }
}
