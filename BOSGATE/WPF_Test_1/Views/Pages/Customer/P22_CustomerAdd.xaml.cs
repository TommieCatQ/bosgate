﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P22_CustomerAdd.xaml
    /// </summary>
    public partial class P22_CustomerAdd : Page
    {
        Customer cust = new Customer();
        DatabaseController svDB = new DatabaseController(1);
        SecurityController secure = new SecurityController();

        public P22_CustomerAdd()
        {
            InitializeComponent();
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void But_Create_Click(object sender, RoutedEventArgs e)
        {
            Text_Name.Background = new SolidColorBrush(Colors.LightGreen);
            Text_CustID.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Phone.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Email.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Password.Background = new SolidColorBrush(Colors.Khaki);
            Text_Password_Rep.Background = new SolidColorBrush(Colors.Khaki);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";
            if (Text_Name.Text.Length < 1)
            {
                Text_Name.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập tên khách hàng";
                return;
            }
            else if (Text_CustID.Text.Length < 1)
            {
                Text_CustID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập mã số khách hàng";
                return;
            }
            else if (Text_Phone.Text.Length < 1)
            {
                Text_Phone.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập số điện thoại";
                return;
            }
            else if ((Text_Email.Text.Length < 1) || (!Text_Email.Text.Contains(".")) || (!Text_Email.Text.Contains("@")))
            {
                Text_Email.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập email đúng";
                return;
            }
            else if (Text_Password.Password.Length < 8)
            {
                Text_Password.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Mật khẩu ít nhất 8 kí tự";
                return;
            }
            else if (Text_Password.Password != Text_Password_Rep.Password)
            {
                Text_Password_Rep.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Mật khẩu không trùng khớp";
                return;
            }

            cust.Name = Text_Name.Text;
            cust.CustomerID = Text_CustID.Text;
            cust.Email = Text_Email.Text;
            cust.Phone = Text_Phone.Text;
            cust.Status = 0;
            cust.Password_hash = secure.Password_Encode(Text_Password.Password);
            cust.Created_at = DateTime.Today;

            try
            {
                svDB.Customer_Insert(cust);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã tạo tài khoản " + cust.CustomerID;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("CustomerID_UNIQUE"))
                    Warning.Text = cust.CustomerID + " đã được tạo rồi";
                svDB.CloseConnection();
            }
        }
    }
}
