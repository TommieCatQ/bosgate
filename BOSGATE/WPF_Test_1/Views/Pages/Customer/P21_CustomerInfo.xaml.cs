﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization;
using System.ComponentModel;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P21_CustomerData.xaml
    /// </summary>
    public partial class P21_CustomerInfo : Page
    {
        private string CustId;
        private Customer cust = new Customer();
        List<Farm> FarmsofCust = new List<Farm>();
        Dictionary<string, int> DevCount_As_FarmID = new Dictionary<string, int>();
        Dictionary<string, int> CowCount_As_FarmID = new Dictionary<string, int>();
        private List<string> StatusList = new List<string>();
        DatabaseController svDB = new DatabaseController(1);

        public P21_CustomerInfo(string custID)
        {
            InitializeComponent();
            CustId = custID;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
            valueList.Add(new KeyValuePair<string, int>("Developer", 60));
            valueList.Add(new KeyValuePair<string, int>("Misc", 20));
            valueList.Add(new KeyValuePair<string, int>("Tester", 50));
            valueList.Add(new KeyValuePair<string, int>("QA", 30));
            valueList.Add(new KeyValuePair<string, int>("Project Manager", 40));


            // Add statuses
            StatusList.Add("Đang hoạt động");
            StatusList.Add("Ngừng");
            Text_Status.ItemsSource = StatusList;

            Load_Tab_Info();
            Load_Tab_ListFarm();
        }

        private void But_EditInfo_Click(object sender, RoutedEventArgs e)
        {
            Text_Name.Background = new SolidColorBrush(Colors.LightGreen);
            Text_CustID.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Phone.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Email.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Status.Background = new SolidColorBrush(Colors.LightGreen);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";
            if (Text_Name.Text.Length < 1)
            {
                Text_Name.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập tên khách hàng";
                return;
            }
            else if (Text_CustID.Text.Length < 1)
            {
                Text_CustID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập mã số khách hàng";
                return;
            }
            else if (Text_Phone.Text.Length < 1)
            {
                Text_Phone.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập số điện thoại";
                return;
            }
            else if ((Text_Email.Text.Length < 1) || (!Text_Email.Text.Contains(".")) || (!Text_Email.Text.Contains("@")))
            {
                Text_Email.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập email đúng";
                return;
            }

            cust.Name = Text_Name.Text;
            cust.CustomerID = Text_CustID.Text;
            cust.Email = Text_Email.Text;
            cust.Phone = Text_Phone.Text;
            cust.Status = 0;
            cust.Created_at = Text_DateIn.SelectedDate.Value;
            cust.Status = Text_Status.SelectedIndex;

            try
            {
                svDB.Customer_Update(cust, CustId);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã cập nhật " + CustId;
                Header_CustName.Text = "KHÁCH HÀNG - " + cust.Name;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("CustomerID_UNIQUE"))
                    Warning.Text = cust.CustomerID + " đã được tạo rồi";
                svDB.CloseConnection();
            }
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (tabControl.SelectedIndex == 1)
            //    Load_Tab_ListFarm();

            //if (tabControl.SelectedIndex == 2)
            //    Load_Tab_Info();
        }

        private void Load_Tab_Info()
        {
            //Load customer info from local DB
            cust = svDB.Customer_SelectOne(CustId);
            Header_CustName.Text = "KHÁCH HÀNG - " + cust.Name;

            // Load Customer Info to Textboxes
            Text_Name.Text = cust.Name;
            Text_CustID.Text = cust.CustomerID;
            Text_Email.Text = cust.Email;
            Text_Phone.Text = cust.Phone;
            Text_DateIn.SelectedDate = Convert.ToDateTime(cust.Created_at);
            Text_Status.SelectedIndex = cust.Status;
        }

        private void Load_Tab_ListFarm()
        {
            //Load list of customer's farms
            FarmsofCust = new List<Farm>();
            FarmsofCust = svDB.Farm_SelectFarmsFromCustomer(cust.CustomerID);

            DevCount_As_FarmID = svDB.Farm_DeviceDict_As_farmid();
            CowCount_As_FarmID = svDB.Farm_CowDict_As_farmid();
            foreach (Farm fa in FarmsofCust)
            {
                try
                {
                    fa.NoDevice = DevCount_As_FarmID[fa.FarmID];
                }
                catch
                { // if there is no key for farmid 
                  //--> no device in that farm
                    fa.NoDevice = 0;
                }

                try
                {
                    fa.NoCow = CowCount_As_FarmID[fa.FarmID];
                }
                catch
                {// if there is no key for farmid 
                 //--> no cow in that farm
                    fa.NoCow = 0;
                }
            }

            dgFarm.ItemsSource = FarmsofCust;

        }
        #region Customer's Farms Tab
        private void dgDevice_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                if (e.Row.GetIndex() % 2 == 0)
                {
                    e.Row.Background = new SolidColorBrush(Colors.White);
                    e.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    e.Row.Background = new SolidColorBrush(Colors.DimGray);
                    e.Row.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        private void TBox_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgFarm.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                Farm p = o as Farm;
                return (p.FarmID.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }

        private void But_AddFarm_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P32_FarmAdd(cust.CustomerID), UriKind.Relative);
        }

        private void dgFarm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Farm choosenFarm = (Farm)dgFarm.SelectedItem;
                this.NavigationService.Navigate(new P31_FarmInfo(choosenFarm.FarmID), UriKind.Relative);
            }
            catch { }
        }
        #endregion

        private void Text_Status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
