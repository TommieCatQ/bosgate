﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P2_DeviceManager.xaml
    /// </summary>
    public partial class P2_CustomerManager : Page
    {
        List<Customer> Customers = new List<Customer>();
        DatabaseController svDB = new DatabaseController(1);

        public P2_CustomerManager()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Customers = new List<Customer>();
            Customers = svDB.Customer_SelectAll();
            dgCustomer.ItemsSource = Customers;
        }

        // khởi tạo Template cho Data Grid
        private void dgCustomer_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                if (e.Row.GetIndex() % 2 == 0)
                {
                    e.Row.Background = new SolidColorBrush(Colors.White);
                    e.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    e.Row.Background = new SolidColorBrush(Colors.DimGray);
                    e.Row.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        private void TBox_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgCustomer.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                Customer p = o as Customer;
                return (p.CustomerID.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }

        private void dgCustomer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Customer choosenOne = (Customer)dgCustomer.SelectedItem;
                this.NavigationService.Navigate(new P21_CustomerInfo(choosenOne.CustomerID), UriKind.Relative);
            }
            catch { }

        }

        private void But_AddCust_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P22_CustomerAdd(), UriKind.Relative);
        }
    }
}
