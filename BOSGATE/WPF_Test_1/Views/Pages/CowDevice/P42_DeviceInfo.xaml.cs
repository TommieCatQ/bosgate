﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P42_DeviceInfo.xaml
    /// </summary>
    public partial class P42_DeviceInfo : Page
    {
        private string DeviceId = "";
        Device dev = new Device();
        List<string> farmIDs = new List<string>();
        List<string> StatusList = new List<string>();
        DatabaseController svDB = new DatabaseController(1);

        public P42_DeviceInfo(string devid)
        {
            InitializeComponent();
            DeviceId = devid;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // Add items to status combo box
            StatusList.Add("Đang hoạt động");
            StatusList.Add("Dừng");
            Text_Status.ItemsSource = StatusList;

            Load_Tab_Info();
        }

        private void But_Update_Click(object sender, RoutedEventArgs e)
        {
            Text_DeviceName.Background = new SolidColorBrush(Colors.LightGreen);
            Text_DeviceID.Background = new SolidColorBrush(Colors.LightGreen);
            Text_FarmID.Background = new SolidColorBrush(Colors.White);
            Text_HWVer.Background = new SolidColorBrush(Colors.LightGreen);
            Text_SWVer.Background = new SolidColorBrush(Colors.LightGreen);
            Text_DateIn.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Status.Background = new SolidColorBrush(Colors.LightGreen);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";
            if (Text_DeviceName.Text.Length < 1)
            {
                Text_DeviceName.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID bò";
                return;
            }
            else if (Text_DeviceID.Text.Length < 1)
            {
                Text_DeviceID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID Tag phải";
                return;
            }
            else if (Text_FarmID.SelectedValue == null)
            {
                Text_FarmID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy chọn Trang trại nuôi";
                return;
            }
            else if (Text_HWVer.Text.Length < 1)
            {
                Text_HWVer.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID Tag trái";
                return;
            }
            else if (Text_SWVer.Text.Length < 1)
            {
                Text_SWVer.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ngày bắt đầu nuôi";
                return;
            }
            else if (Text_DateIn.Text.Length < 1)
            {
                Text_DateIn.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập cân nặng khởi điểm";
                return;
            }

            dev.DeviceName = Text_DeviceName.Text;
            dev.DeviceID = Text_DeviceID.Text;
            dev.FarmID = Text_FarmID.Text;
            dev.HWVer = Text_HWVer.Text;
            dev.SWVer = Text_SWVer.Text;
            dev.Created_at = Text_DateIn.SelectedDate.Value;
            dev.Status = Text_Status.SelectedIndex;

            try
            {
                svDB.Device_Update(dev, DeviceId);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã cập nhật " + dev.DeviceID;
                DeviceId = dev.DeviceID;
                Header_DeviceID.Text = "THIẾT BỊ - " + dev.DeviceName;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("DeviceID_UNIQUE"))
                    Warning.Text = dev.DeviceID + " đã được tạo rồi";
                svDB.CloseConnection();
            }
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tabControl.SelectedIndex == 1)
                Load_Tab_Info();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Load_Tab_Info()
        {
            // Load farmID
            farmIDs = svDB.Farm_SelectFarmIDs();
            Text_FarmID.ItemsSource = farmIDs;

            // Load Device Info from local DB
            dev = svDB.Device_SelectOne(DeviceId);

            // Load Device Info to text boxes
            Text_DeviceName.Text = dev.DeviceName;
            Text_DeviceID.Text = dev.DeviceID;
            Text_FarmID.Text = dev.FarmID;
            Text_HWVer.Text = dev.HWVer;
            Text_SWVer.Text = dev.SWVer;
            Text_DateIn.SelectedDate = dev.Created_at;
            Text_Status.SelectedIndex = dev.Status;
            Header_DeviceID.Text = "THIẾT BỊ - " + dev.DeviceName;
        }

        private void Text_FarmID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void Text_Status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
