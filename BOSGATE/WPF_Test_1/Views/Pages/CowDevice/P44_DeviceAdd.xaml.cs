﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P44_DeviceAdd.xaml
    /// </summary>
    public partial class P44_DeviceAdd : Page
    {
        private string FarmId = "";
        Device dev = new Device();
        List<string> farmIDs = new List<string>();
        DatabaseController svDB = new DatabaseController(1);

        public P44_DeviceAdd(string farmid)
        {
            InitializeComponent();
            FarmId = farmid;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            farmIDs = svDB.Farm_SelectFarmIDs();
            Text_FarmID.ItemsSource = farmIDs;
            Text_FarmID.SelectedValue = FarmId;
        }

        private void But_Create_Click(object sender, RoutedEventArgs e)
        {
            Text_DeviceName.Background = new SolidColorBrush(Colors.LightGreen);
            Text_DeviceID.Background = new SolidColorBrush(Colors.LightGreen);
            Text_FarmID.Background = new SolidColorBrush(Colors.White);
            Text_HWVer.Background = new SolidColorBrush(Colors.LightGreen);
            Text_SWVer.Background = new SolidColorBrush(Colors.LightGreen);
            Text_DateIn.Background = new SolidColorBrush(Colors.LightGreen);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";
            if (Text_DeviceName.Text.Length < 1)
            {
                Text_DeviceName.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID bò";
                return;
            }
            else if (Text_DeviceID.Text.Length < 1)
            {
                Text_DeviceID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID Tag phải";
                return;
            }
            else if (Text_FarmID.SelectedValue == null)
            {
                Text_FarmID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy chọn Trang trại nuôi";
                return;
            }
            else if (Text_HWVer.Text.Length < 1)
            {
                Text_HWVer.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID Tag trái";
                return;
            }
            else if (Text_SWVer.Text.Length < 1)
            {
                Text_SWVer.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ngày bắt đầu nuôi";
                return;
            }
            else if (Text_DateIn.Text.Length < 1)
            {
                Text_DateIn.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập cân nặng khởi điểm";
                return;
            }

            dev.DeviceName = Text_DeviceName.Text;
            dev.DeviceID = Text_DeviceID.Text;
            dev.FarmID = Text_FarmID.Text;
            dev.HWVer = Text_HWVer.Text;
            dev.SWVer = Text_SWVer.Text;
            dev.Created_at = Text_DateIn.SelectedDate.Value;
            dev.Status = 0;

            try
            {
                svDB.Device_Insert(dev);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã tạo " + dev.DeviceID;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("DeviceID_UNIQUE"))
                    Warning.Text = dev.DeviceID + " đã được tạo rồi";
                svDB.CloseConnection();
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

    }
}
