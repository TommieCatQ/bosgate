﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P4_CowAdd.xaml
    /// </summary>
    public partial class P43_CowAdd : Page
    {
        private string FarmId = "";
        Cow cow = new Cow();
        List<string> farmIDs = new List<string>();
        DatabaseController svDB = new DatabaseController(1);

        public P43_CowAdd(string farmid)
        {
            InitializeComponent();
            FarmId = farmid;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            farmIDs = svDB.Farm_SelectFarmIDs();
            Text_FarmID.ItemsSource = farmIDs;
            if (FarmId != "") Text_FarmID.SelectedValue = FarmId;
        }

        private void But_Create_Click(object sender, RoutedEventArgs e)
        {
            Text_CowID.Background = new SolidColorBrush(Colors.LightGreen);
            Text_FarmID.Background = new SolidColorBrush(Colors.White);
            Text_TagRight.Background = new SolidColorBrush(Colors.LightGreen);
            Text_TagLeft.Background = new SolidColorBrush(Colors.LightGreen);
            Text_DateIn.Background = new SolidColorBrush(Colors.LightGreen);
            Text_WeightOrg.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Kind.Background = new SolidColorBrush(Colors.LightGreen);
            Text_DayOld.Background = new SolidColorBrush(Colors.LightGreen);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";
            if (Text_CowID.Text.Length < 1)
            {
                Text_CowID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID bò";
                return;
            }
            else if (Text_FarmID.SelectedValue == null)
            {
                Text_FarmID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy chọn Trang trại nuôi";
                return;
            }
            else if (Text_TagRight.Text.Length < 1)
            {
                Text_TagRight.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID Tag phải";
                return;
            }
            else if (Text_TagLeft.Text.Length < 1)
            {
                Text_TagLeft.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ID Tag trái";
                return;
            }
            else if (Text_DateIn.Text.Length < 1)
            {
                Text_DateIn.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ngày bắt đầu nuôi";
                return;
            }
            else if (Text_WeightOrg.Text.Length < 1)
            {
                Text_WeightOrg.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập cân nặng khởi điểm";
                return;
            }
            else if (Text_Kind.Text.Length < 1)
            {
                Text_Kind.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập giống bò";
                return;
            }
            else if (Text_DayOld.Text.Length < 1)
            {
                Text_DayOld.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập tuổi bò";
                return;
            }


            cow.CowID = Text_CowID.Text;
            cow.FarmID = Text_FarmID.Text;
            cow.TagID_Right = Text_TagRight.Text;
            cow.TagID_Left = Text_TagLeft.Text;
            cow.Date_In = Text_DateIn.SelectedDate.Value;
            cow.Weight_Org = Convert.ToDouble(Text_WeightOrg.Text);
            cow.Breed_ID = Text_Kind.Text;
            cow.Day_Old = Convert.ToInt32(Text_DayOld.Text);
            cow.Food_Digest = 0;
            cow.Eff_Breeding = 0;
            cow.Status = 0;

            try
            {
                svDB.Cow_Insert(cow);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã tạo " + cow.CowID;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("cowid_UNIQUE"))
                    Warning.Text = cow.CowID + " đã được tạo rồi";
                svDB.CloseConnection();
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

    }
}
