﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P4_CowManager.xaml
    /// </summary>
    public partial class P4_DeviceCowManager : Page
    {
        List<Cow> Cows = new List<Cow>();
        List<Device> Devices = new List<Device>();

        DatabaseController svDB = new DatabaseController(1);
        public P4_DeviceCowManager()
        {
            InitializeComponent();
        }

        private void Grid_Device_Loaded(object sender, RoutedEventArgs e)
        {
            Devices = new List<Device>();
            Devices = svDB.Device_SelectAll();
            dgDevice.ItemsSource = Devices;

        }

        private void Grid_Cow_Loaded(object sender, RoutedEventArgs e)
        {
            Cows = new List<Cow>();
            Cows = svDB.Cow_SelectAll();
            dgCow.ItemsSource = Cows;
        }


        private void LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                if (e.Row.GetIndex() % 2 == 0)
                {
                    e.Row.Background = new SolidColorBrush(Colors.White);
                    e.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    e.Row.Background = new SolidColorBrush(Colors.DimGray);
                    e.Row.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        private void TBox_Cow_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgCow.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                Cow p = o as Cow;
                return (p.CowID.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }

        private void But_AddCow_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P43_CowAdd(""), UriKind.Relative);
        }

        private void dgCow_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cow choosenCow = (Cow)dgCow.SelectedItem;
                this.NavigationService.Navigate(new P41_CowInfo(choosenCow.CowID), UriKind.Relative);
            }
            catch { }

        }

        private void TBox_Device_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgDevice.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                Device p = o as Device;
                return (p.DeviceID.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }


        private void But_AddDevice_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P44_DeviceAdd(""), UriKind.Relative);
        }


        private void dgDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Device choosenDev = (Device)dgDevice.SelectedItem;
                this.NavigationService.Navigate(new P42_DeviceInfo(choosenDev.DeviceID), UriKind.Relative);
            }
            catch { }
        }
    }
}
