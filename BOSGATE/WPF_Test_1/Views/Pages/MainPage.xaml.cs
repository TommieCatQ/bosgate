﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    /// 

    public partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void NavigationService_Navigated(object sender, NavigationEventArgs e)
        {
        }

        private void but_User_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P1_UserManager(), UriKind.Relative);
        }

        private void but_Customer_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P2_CustomerManager(), UriKind.Relative);
        }

        private void but_Farm_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P3_FarmManager(), UriKind.Relative);
        }

        private void but_CowDev_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P4_DeviceCowManager(), UriKind.Relative);
        }

        private void but_Standard_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new PageStandard(), UriKind.Relative);
        }

        private void but_Notification_Click(object sender, RoutedEventArgs e)
        {

        }

        private void but_Report_Click(object sender, RoutedEventArgs e)
        {

        }

        private void but_User_MouseEnter(object sender, MouseEventArgs e)
        {
            StackPanel Stack = new StackPanel();
            TextBlock headerBlock = new TextBlock();
            headerBlock.Inlines.Add("User:");
            headerBlock.FontFamily = new FontFamily("Vernada");
            headerBlock.Inlines.Add(new LineBreak());
            headerBlock.FontSize = 22;

            TextBlock bodyBlock = new TextBlock();
            bodyBlock.Inlines.Add(" - Thông tin User");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Cập nhật User");
            bodyBlock.FontFamily = new FontFamily("Vernada");
            bodyBlock.FontSize = 15;

            Stack.Children.Add(headerBlock);
            Stack.Children.Add(bodyBlock);

            but_User.Content = Stack;
        }

        private void but_User_MouseLeave(object sender, MouseEventArgs e)
        {
            but_User.Content = "User";
        }

        private void but_Customer_MouseEnter(object sender, MouseEventArgs e)
        {
            StackPanel Stack = new StackPanel();
            TextBlock headerBlock = new TextBlock();
            headerBlock.Inlines.Add("Khách hàng:");
            headerBlock.FontFamily = new FontFamily("Vernada");
            headerBlock.Inlines.Add(new LineBreak());
            headerBlock.FontSize = 22;

            TextBlock bodyBlock = new TextBlock();
            bodyBlock.Inlines.Add(" - Thông tin chi tiết");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Trang trại của Khách hàng");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Cập nhật thông tin");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Tạo Khách hàng");
            bodyBlock.FontFamily = new FontFamily("Vernada");
            bodyBlock.FontSize = 15;

            Stack.Children.Add(headerBlock);
            Stack.Children.Add(bodyBlock);

            but_Customer.Content = Stack;
        }

        private void but_Customer_MouseLeave(object sender, MouseEventArgs e)
        {
            but_Customer.Content = "Khách hàng";
        }

        private void but_Farm_MouseEnter(object sender, MouseEventArgs e)
        {
            StackPanel Stack = new StackPanel();
            TextBlock headerBlock = new TextBlock();
            headerBlock.Inlines.Add("Trang trại:");
            headerBlock.FontFamily = new FontFamily("Vernada");
            headerBlock.Inlines.Add(new LineBreak());
            headerBlock.FontSize = 22;

            TextBlock bodyBlock = new TextBlock();
            bodyBlock.Inlines.Add(" - Thông tin chi tiết");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Thiết bị của Trang trại");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Bò của Trang trại");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Cập nhật thông tin");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Tạo Trang trại mới");
            bodyBlock.FontFamily = new FontFamily("Vernada");
            bodyBlock.FontSize = 15;

            Stack.Children.Add(headerBlock);
            Stack.Children.Add(bodyBlock);

            but_Farm.Content = Stack;
        }

        private void but_Farm_MouseLeave(object sender, MouseEventArgs e)
        {
            but_Farm.Content = "Trang trại";
        }

        private void but_CowDev_MouseEnter(object sender, MouseEventArgs e)
        {
            StackPanel Stack = new StackPanel();
            TextBlock headerBlock = new TextBlock();
            headerBlock.Inlines.Add("Thiết bị, Bò:");
            headerBlock.FontFamily = new FontFamily("Vernada");
            headerBlock.Inlines.Add(new LineBreak());
            headerBlock.FontSize = 22;

            TextBlock bodyBlock = new TextBlock();
            bodyBlock.Inlines.Add(" - Thông tin chi tiết");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Cập nhật thông tin");
            bodyBlock.Inlines.Add(new LineBreak());
            bodyBlock.Inlines.Add(" - Tạo Thiết bị/ Bò");
            bodyBlock.FontFamily = new FontFamily("Vernada");
            bodyBlock.FontSize = 15;

            Stack.Children.Add(headerBlock);
            Stack.Children.Add(bodyBlock);

            but_CowDev.Content = Stack;
        }

        private void but_CowDev_MouseLeave(object sender, MouseEventArgs e)
        {
            but_CowDev.Content = "Thiết bị, Bò";
        }

    }
}
