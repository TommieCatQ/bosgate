﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P31_FarmData.xaml
    /// </summary>
    public partial class P31_FarmInfo : Page
    {
        private string FarmId = "";
        private Farm farm = new Farm();
        private List<string> StatusList = new List<string>();
        private List<string> custIDs = new List<string>();
        private List<Cow> Farm_Cows = new List<Cow>();
        private List<Device> Farm_Devices = new List<Device>();
        DatabaseController svDB = new DatabaseController(1);

        public P31_FarmInfo(string farmid)
        {
            InitializeComponent();
            FarmId = farmid;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            // Text graph

            //Load List of Status
            StatusList.Add("Đang hoạt động");
            StatusList.Add("Ngừng");
            Text_Status.ItemsSource = StatusList;

            Load_Tab_Info();
            Load_Tab_ListCow();
            Load_Tab_ListDevice();
        }

        private void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (tabControl.SelectedIndex == 1)
            //    Load_Tab_ListCow();

            //if (tabControl.SelectedIndex == 2)
            //    Load_Tab_ListDevice();

            //if (tabControl.SelectedIndex == 3)
            //    Load_Tab_Info();
        }

        private void But_EditInfo_Click(object sender, RoutedEventArgs e)
        {
            Text_Name.Background = new SolidColorBrush(Colors.LightGreen);
            Text_FarmID.Background = new SolidColorBrush(Colors.LightGreen);
            Text_CustID.Background = new SolidColorBrush(Colors.White);
            Text_Location.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Area.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Status.Background = new SolidColorBrush(Colors.LightGreen);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";
            if (Text_Name.Text.Length < 1)
            {
                Text_Name.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập tên khách hàng";
                return;
            }
            else if (Text_FarmID.Text.Length < 1)
            {
                Text_FarmID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập mã trang trại";
                return;
            }
            else if (Text_CustID.SelectedValue == null)
            {
                Text_CustID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy chọn Khách hàng chủ quản";
                return;
            }
            else if (Text_Location.Text.Length < 1)
            {
                Text_Location.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập địa điểm trang trại";
                return;
            }
            else if (Text_Area.Text.Length < 1)
            {
                Text_Area.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập diện tích trang trại";
                return;
            }

            farm.FarmName = Text_Name.Text;
            farm.FarmID = Text_FarmID.Text;
            farm.CustomerID = Text_CustID.Text;
            farm.Location = Text_Location.Text;
            farm.AreaSize = Convert.ToDouble(Text_Area.Text);
            farm.Status = Text_Status.SelectedIndex;

            try
            {
                svDB.Farm_Update(farm, FarmId);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã cập nhật " + farm.FarmID;
                Header_FarmName.Text = "TRANG TRẠI - " + farm.FarmName;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("FarmID_UNIQUE"))
                    Warning.Text = farm.FarmID + " đã được tạo rồi";
                svDB.CloseConnection();
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                if (e.Row.GetIndex() % 2 == 0)
                {
                    e.Row.Background = new SolidColorBrush(Colors.White);
                    e.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    e.Row.Background = new SolidColorBrush(Colors.DimGray);
                    e.Row.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        private void Load_Tab_Info()
        {
            //Load farm info
            farm = svDB.Farm_SelectOne(FarmId);

            //Load Page Header
            Header_FarmName.Text = "TRANG TRẠI - " + farm.FarmName;

            //Load customer list
            custIDs = svDB.Customer_SelectCustIDs();
            Text_CustID.ItemsSource = custIDs;
            Text_CustID.SelectedValue = farm.CustomerID;

            //Load number of device and cow
            Text_NoDevice.Text = farm.NoDevice.ToString();
            Text_NoCow.Text = farm.NoCow.ToString();

            // Load Text boxes
            Text_Name.Text = farm.FarmName;
            Text_FarmID.Text = farm.FarmID;
            Text_CustID.Text = farm.CustomerID;
            Text_Location.Text = farm.Location;
            Text_Area.Text = farm.AreaSize.ToString();
            Text_Status.SelectedIndex = farm.Status;
        }

        private void Load_Tab_ListCow()
        {
            Farm_Cows = new List<Cow>();
            Farm_Cows = svDB.Cow_SelectFromFarmID(FarmId);
            dgCow.ItemsSource = Farm_Cows;
        }

        private void TBox_Cow_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgCow.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                Cow p = o as Cow;
                return (p.CowID.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }

        private void But_AddCow_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P43_CowAdd(FarmId), UriKind.Relative);
        }

        private void dgCow_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cow choosenCow = (Cow)dgCow.SelectedItem;
                this.NavigationService.Navigate(new P41_CowInfo(choosenCow.CowID), UriKind.Relative);
            }
            catch { }

        }

        private void Load_Tab_ListDevice()
        {
            Farm_Devices = new List<Device>();
            Farm_Devices = svDB.Device_SelectFromFarmID(FarmId);
            dgDevice.ItemsSource = Farm_Devices;
        }

        private void TBox_Device_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgDevice.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                Device p = o as Device;
                return (p.DeviceID.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }

        private void But_AddDevice_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P44_DeviceAdd(FarmId), UriKind.Relative);
        }

        private void dgDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Device choosenDev = (Device)dgDevice.SelectedItem;
                this.NavigationService.Navigate(new P42_DeviceInfo(choosenDev.DeviceID), UriKind.Relative);
            }
            catch { }
        }

        private void Text_CustID_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }

        private void Text_Status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
        }
    }
}
