﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P32_FarmAdd.xaml
    /// </summary>
    public partial class P32_FarmAdd : Page
    {
        Farm farm = new Farm();
        string NavigatedFromCustomer;
        List<string> custIDs= new List<string>();
        DatabaseController svDB = new DatabaseController(1);
        SecurityController secure = new SecurityController();

        public P32_FarmAdd(string CustomerID)
        {
            InitializeComponent();
            NavigatedFromCustomer = CustomerID;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            custIDs = svDB.Customer_SelectCustIDs();
            Text_CustID.ItemsSource = custIDs;

            // if navigated from a specific customer
            // create this default setting
            if (NavigatedFromCustomer!="")
                Text_CustID.SelectedValue = NavigatedFromCustomer;
        }

        private void But_Create_Click(object sender, RoutedEventArgs e)
        {
            Text_Name.Background = new SolidColorBrush(Colors.LightGreen);
            Text_FarmID.Background = new SolidColorBrush(Colors.LightGreen);
            Text_CustID.Background = new SolidColorBrush(Colors.White);
            Text_Location.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Area.Background = new SolidColorBrush(Colors.LightGreen);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";
            if (Text_Name.Text.Length < 1)
            {
                Text_Name.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập tên khách hàng";
                return;
            }
            else if (Text_FarmID.Text.Length < 1)
            {
                Text_FarmID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập mã trang trại";
                return;
            }
            else if (Text_CustID.SelectedValue == null)
            {
                Text_CustID.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy chọn Khách hàng chủ quản";
                return;
            }
            else if (Text_Location.Text.Length < 1)
            {
                Text_Location.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập địa điểm trang trại";
                return;
            }
            else if (Text_Area.Text.Length < 1)
            {
                Text_Area.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập diện tích trang trại";
                return;
            }

            farm.FarmName = Text_Name.Text;
            farm.FarmID = Text_FarmID.Text;
            farm.CustomerID = Text_CustID.Text;
            farm.Location = Text_Location.Text;
            farm.AreaSize = Convert.ToDouble(Text_Area.Text);
            farm.Status = 0;

            try
            {
                svDB.Farm_Insert(farm);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã tạo tài khoản " + farm.FarmID;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("FarmID_UNIQUE"))
                    Warning.Text = farm.FarmID + " đã được tạo rồi";
                svDB.CloseConnection();
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
