﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BOSGATE.Properties;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LogInPage : Page
    {
        MainPage slcPage = new MainPage();
        P12_RegisterPage regPage = new P12_RegisterPage();
        DatabaseController svDB = new DatabaseController(1);
        SecurityController secure = new SecurityController();
        Settings settings = Settings.Default;
        List<User> users = new List<User>();

        public LogInPage()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            users = svDB.User_SelectAll();
            tBox_User.Text = settings.UserName;
        }

        private void but_Start_Click(object sender, RoutedEventArgs e)
        {
            CheckLogIn();
        }

        // User can also press enter to proceed
        private void Grid_General_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) CheckLogIn();
        }


        private void but_Register_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(regPage, UriKind.Relative);
        }

        // Check log in credentials
        private void CheckLogIn()
        {
            Text_Warning.Text = "";
            // check if this user is valid
            try
            {
                string password_hash = users.Find(x => x.Email == tBox_User.Text).Password_hash;
                string PWBox_hash = secure.Password_Encode(tBox_Pass.Password);
                if (PWBox_hash == password_hash)
                {
                    settings.UserName = tBox_User.Text;
                    settings.Save();
                    this.NavigationService.Navigate(slcPage, UriKind.Relative);
                }
                else
                {
                    Text_Warning.Text = "Sai mật khẩu";
                }
            }
            catch
            {
                // This exception occur when the Email is not valid in the database
                Text_Warning.Text = "Email chưa đăng kí";

                // TEST ONLY
                //this.NavigationService.Navigate(slcPage, UriKind.Relative);
                users = svDB.User_SelectAll();
            }
        }
    }
}
