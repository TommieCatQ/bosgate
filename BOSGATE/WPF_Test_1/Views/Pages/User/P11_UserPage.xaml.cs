﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P11_UserPage.xaml
    /// </summary>
    public partial class P11_UserPage : Page
    {
        int UserID;
        string oldEmail = "";
        User choosenUser;
        DatabaseController svDB = new DatabaseController(1);
        SecurityController secure = new SecurityController();
        List<string> Positions = new List<string>();
        bool ChangeState = false;

        public P11_UserPage(int Id)
        {
            InitializeComponent();
            Positions = new List<string>();
            Positions.Add("Quản lí");
            Positions.Add("Nhân viên");
            Positions.Add("Kĩ thuật viên");
            Text_Position.ItemsSource = Positions;

            UserID = Id;
        }

        private void LoadUser()
        {
            choosenUser = svDB.User_SelectOne(UserID);
            Text_Name.Text = choosenUser.Username;
            Text_Position.SelectedIndex = choosenUser.Position;
            Text_EmpId.Text = choosenUser.EmpID.ToString();
            Text_Manager.Text = choosenUser.DirectManager;
            Text_Birthday.Text = choosenUser.Birthday;
            Text_Created_at.Text = choosenUser.Created_at.ToString("dd-MM-yyyy");
            Text_Email.Text = choosenUser.Email;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            LoadUser();
        }

        private void But_Update_Click(object sender, RoutedEventArgs e)
        {
            Text_Name.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Position.Background = new SolidColorBrush(Colors.White);
            Text_EmpId.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Email.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Manager.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Birthday.Background = new SolidColorBrush(Colors.White);
            oldEmail = choosenUser.Email;

            if (Text_Name.Text.Length < 6)
            {
                Text_Name.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Tài khoản có ít nhất 6 kí tự";
                return;
            }
            else if (Text_Position.SelectedValue == null)
            {
                Text_Position.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy chọn 1 chức vụ";
                return;
            }
            else if (Text_EmpId.Text.Length < 1)
            {
                Text_EmpId.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập mã nhân viên";
                return;
            }
            else if ((Text_Email.Text.Length < 1) || (!Text_Email.Text.Contains("."))||(!Text_Email.Text.Contains("@")))
            {
                Text_EmpId.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập email đúng";
                return;
            }
            else if (Text_Manager.Text.Length < 1)
            {
                Text_Manager.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Tên quản lí trực tiếp của user này";
                return;
            }
            else if (Text_Birthday.Text.Length < 1)
            {
                Text_Birthday.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ngày sinh";
                return;
            }
            choosenUser.Username = Text_Name.Text;
            choosenUser.Position = Text_Position.SelectedIndex;
            choosenUser.PositionName = Text_Position.SelectedValue.ToString();
            choosenUser.EmpID = Convert.ToInt32(Text_EmpId.Text);
            choosenUser.DirectManager = Text_Manager.Text;
            choosenUser.Birthday = Text_Birthday.Text;
            choosenUser.Created_at = Text_Created_at.SelectedDate.Value;
            choosenUser.Email = Text_Email.Text;

            // Update local
            try
            {
                svDB.User_Update(choosenUser, oldEmail);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã cập nhật cho " + choosenUser.Username;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("name_UNIQUE"))
                    Warning.Text = choosenUser.Username + " đã được tạo rồi";
                if (ex.Message.Contains("empid_UNIQUE"))
                    Warning.Text = "ID nhân viên đã tồn tại";
                svDB.CloseConnection();
            }

            // Update server - to be done in the future

            ChangeState = false;
            But_En_Dis.Background = new SolidColorBrush(Colors.DarkCyan);
            But_En_Dis.Content = "Mở thay đổi";
            StPanel_General.IsEnabled = false;

        }

        private void But_En_Dis_Click(object sender, RoutedEventArgs e)
        {
            if (ChangeState == false)
            {
                ChangeState = true;
                But_En_Dis.Background = new SolidColorBrush(Colors.OrangeRed);
                But_En_Dis.Content = "Khóa thay đổi";
                StPanel_General.IsEnabled = true;
            }
            else
            {
                ChangeState = false;
                But_En_Dis.Background = new SolidColorBrush(Colors.DarkCyan);
                But_En_Dis.Content = "Mở thay đổi";
                StPanel_General.IsEnabled = false;
                LoadUser();
            }
        }

        private void But_Update_Pass_Click(object sender, RoutedEventArgs e)
        {
            Text_Password_Old.Background = new SolidColorBrush(Colors.Khaki);
            Text_Password.Background = new SolidColorBrush(Colors.Khaki);
            Text_Password_Rep.Background = new SolidColorBrush(Colors.Khaki);
            Warning_Pass.Foreground = new SolidColorBrush(Colors.Red);
            Warning_Pass.Text = "";

            if (secure.Password_Encode(Text_Password_Old.Password) != choosenUser.Password_hash)
            {
                Text_Password_Old.Background = new SolidColorBrush(Colors.IndianRed);
                Warning_Pass.Text = "Mật khẩu cũ không đúng";
                return;
            }
            else if (Text_Password.Password.Length < 8)
            {
                Text_Password.Background = new SolidColorBrush(Colors.IndianRed);
                Warning_Pass.Text = "Mật khẩu ít nhất 8 kí tự";
                return;
            }
            else if (Text_Password.Password != Text_Password_Rep.Password)
            {
                Text_Password_Rep.Background = new SolidColorBrush(Colors.IndianRed);
                Warning_Pass.Text = "Mật khẩu không trùng khớp";
                return;
            }

            try
            {
                svDB.User_ChangePassword(choosenUser.Email, secure.Password_Encode(Text_Password.Password));
                Warning_Pass.Foreground = new SolidColorBrush(Colors.Green);
                Warning_Pass.Text = "Đã đổi mật khẩu";
            }
            catch (Exception ex)
            {
                svDB.CloseConnection();
            }

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

    }
}
