﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P12_RegisterPage.xaml
    /// </summary>
    public partial class P12_RegisterPage : Page
    {
        User user = new User();
        List<string> Positions = new List<string>();
        DatabaseController svDB = new DatabaseController(1);
        SecurityController secure = new SecurityController();

        public P12_RegisterPage()
        {
            InitializeComponent();
            Positions = new List<string>();
            Positions.Add("Quản lí");
            Positions.Add("Nhân viên");
            Positions.Add("Kĩ thuật viên");
            Text_Position.ItemsSource = Positions;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void But_Create_Click(object sender, RoutedEventArgs e)
        {
            Text_Name.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Position.Background = new SolidColorBrush(Colors.White);
            Text_EmpId.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Email.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Manager.Background = new SolidColorBrush(Colors.LightGreen);
            Text_Birthday.Background = new SolidColorBrush(Colors.White);
            Text_Password.Background = new SolidColorBrush(Colors.Khaki);
            Text_Password_Rep.Background = new SolidColorBrush(Colors.Khaki);
            Warning.Foreground = new SolidColorBrush(Colors.Red);
            Warning.Text = "";

            if (Text_Name.Text.Length < 6)
            {
                Text_Name.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Tài khoản có ít nhất 6 kí tự";
                return;
            }
            else if (Text_Position.SelectedValue == null)
            {
                Text_Position.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy chọn 1 chức vụ";
                return;
            }
            else if (Text_EmpId.Text.Length < 1)
            {
                Text_EmpId.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập mã nhân viên";
                return;
            }
            else if ((Text_Email.Text.Length < 1) || (!Text_Email.Text.Contains(".")) || (!Text_Email.Text.Contains("@")))
            {
                Text_Email.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập email đúng";
                return;
            }
            else if (Text_Manager.Text.Length < 1)
            {
                Text_Manager.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Tên quản lí trực tiếp của user này";
                return;
            }
            else if (Text_Birthday.Text.Length < 1)
            {
                Text_Birthday.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Hãy nhập ngày sinh";
                return;
            }
            else if (Text_Password.Password.Length < 8)
            {
                Text_Password.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Mật khẩu ít nhất 8 kí tự";
                return;
            }
            else if (Text_Password.Password != Text_Password_Rep.Password)
            {
                Text_Password_Rep.Background = new SolidColorBrush(Colors.IndianRed);
                Warning.Text = "Mật khẩu không trùng khớp";
                return;
            }

            user.Username = Text_Name.Text;
            user.Position = Text_Position.SelectedIndex;
            user.PositionName = Text_Position.SelectedValue.ToString();
            user.EmpID = Convert.ToInt32(Text_EmpId.Text);
            user.DirectManager = Text_Manager.Text;
            user.Birthday = Text_Birthday.Text;
            user.Password_hash = secure.Password_Encode(Text_Password.Password);
            user.Email = Text_Email.Text;
            user.Api_key = "no";
            user.Created_at = DateTime.Today;

            try
            {
                svDB.User_Insert(user);
                Warning.Foreground = new SolidColorBrush(Colors.Green);
                Warning.Text = "Đã tạo tài khoản " + user.Username;
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("name_UNIQUE"))
                    Warning.Text = user.Username + " đã được tạo rồi";
                if (ex.Message.Contains("empid_UNIQUE"))
                    Warning.Text ="ID nhân viên đã tồn tại";
                svDB.CloseConnection();
            }

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9.]+");
            e.Handled = regex.IsMatch(e.Text);
        }

    }
}
