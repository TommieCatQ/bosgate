﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DatabaseController lcData = new DatabaseController(0);
        DatabaseController svDB = new DatabaseController(1);

        public MainWindow()
        {
            InitializeComponent();
            List<User> users = lcData.User_SelectAll();
            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(DailiesUpdate);
            dispatcherTimer.Interval = new TimeSpan(0,0,5);
            dispatcherTimer.Start();

            HostingFrame.NavigationService.Navigating += NavigationService_Navigating;
            HostingFrame.NavigationService.Navigated += NavigationService_Navigated;
        }

        private void NavigationService_Navigated(object sender, NavigationEventArgs e)
        {
            // Remove LogInPage in the History
            if (HostingFrame.Content.ToString() == "BOSGATE.MainPage")
            {
                HostingFrame.RemoveBackEntry();
            }

            if (HostingFrame.Content.ToString() == "BOSGATE.LogInPage")
            {
                while(HostingFrame.CanGoBack)
                {
                    HostingFrame.RemoveBackEntry();
                }
            }   

            // Enable/Disable Back Button
            if (HostingFrame.CanGoBack) But_Back.IsEnabled = true;
            else But_Back.IsEnabled = false;

            // Enable/Disable Forward Button
            if (HostingFrame.CanGoForward) But_For.IsEnabled = true;
            else But_For.IsEnabled = false;
        }

        private void NavigationService_Navigating(object sender, NavigatingCancelEventArgs e)
        {
        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LogInPage loginPage = new LogInPage();
            HostingFrame.NavigationService.Navigate(loginPage);
        }

        private void But_Back_Click(object sender, RoutedEventArgs e)
        {
            if (HostingFrame.NavigationService.CanGoBack)
                HostingFrame.NavigationService.GoBack();

        }

        private void But_For_Click(object sender, RoutedEventArgs e)
        {
            if (HostingFrame.NavigationService.CanGoForward)
                HostingFrame.NavigationService.GoForward();
        }

        private void But_Home_Click(object sender, RoutedEventArgs e)
        {
            string currentPlace = HostingFrame.Content.ToString();
            if ((currentPlace != "BOSGATE.LogInPage")&&(currentPlace != "BOSGATE.P12_RegisterPage"))
                HostingFrame.NavigationService.Navigate(new MainPage(), UriKind.Relative);
        }

        private void But_LogOut_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Bạn có muốn đăng xuất?", "Xác nhận", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                HostingFrame.Navigate(new LogInPage(), UriKind.Relative);
            }
        }

        private void DailiesUpdate(object sender, EventArgs e)
        {
            // update Daily Cow Table for the cycle of 20minutes
            GlobalPassing.dailyCows = svDB.DailyCow_SelectAll();

            // update Daily Device Table for the cycle of 20minutes
            GlobalPassing.dailyDevs = svDB.DailyDevice_SelectAll();
        }

    }
}
